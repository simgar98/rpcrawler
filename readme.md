# RPCrawler
This is the source code of a small scale web-scraping project, as part of a bachelor thesis.
This code comes with very little instructions and no guarantees of anything working.

The purpose of this project is to scrape webpages using a control and an experiment connection, 
and to compare these results to detect website-based blocking. It is designed to be used with JAP/AN.ON,
but can easily be made to work with any HTTP-proxy based experiment connection. 

## Setup/Usage
1. Clone the repo <br>
2. Create a MySQL database <br>
3. Import the repo in your favorite IDE using Maven <br>
4. Change database credentials in the Constants.java class <br>
5. Build the project using Maven <br>
6. Run the RPCrawler.jar file using Java, from the root directory of the project <br>
7. Explore the available commands and use them as desired...

## Database
#### Creation
```MySQL
create database rpcrawler;
use rpcrawler;
```

Fill the password, username, and hostname in as applicable.
```MySQL
grant all on rpcrawler.* to '<user>'@'<host>' identified by '<password>';
```

To use the details present in the Constants class by default:
```MySQL
grant all on rpcrawler.* to 'rpcrawleruser'@'localhost' identified by 'sdf9128743khasf85sfal8';
```

#### Table setup
Table: webpages: stores info about domain-url, maps to ID
```MySQL
CREATE TABLE webpages (id int AUTO_INCREMENT, url VARCHAR(256), domain VARCHAR(128), PRIMARY KEY (id));
```

Table scrapes: stores info about all the scrapes
```MySQL
CREATE TABLE scrapes (scrape_id int AUTO_INCREMENT, webpage_id int, timestamp TIMESTAMP, config int, title VARCHAR(256), content MEDIUMTEXT, screenshot VARCHAR(256), time_taken int, PRIMARY KEY (scrape_id), FOREIGN KEY (webpage_id) REFERENCES webpages(id));
```
 
Table results: Stores info about the compared scrapes
```MySQL
CREATE TABLE results (scrape_id_a int, scrape_id_b int, verdict VARCHAR(128), block_types VARCHAR(2048), compared_images VARCHAR(128), PRIMARY KEY (scrape_id_a, scrape_id_b), FOREIGN KEY (scrape_id_a) REFERENCES scrapes(scrape_id), FOREIGN KEY (scrape_id_b) REFERENCES scrapes(scrape_id));
```

Table manual: Manual verification table
```MySQL
CREATE TABLE manual (scrape_id_a int, scrape_id_b int, verdict VARCHAR(128), timestamp timestamp, primary key (scrape_id_a, scrape_id_b));
```

View results_verified: Results table with some data from other tables. Could also be done with some join statements, which would likely run faster in bigger datasets.
```MySQL 
CREATE VIEW results_verified AS (SELECT *, (SELECT verdict FROM manual as b WHERE a.scrape_id_a = b.scrape_id_a AND a.scrape_id_b = b.scrape_id_b) as manual, (SELECT timestamp FROM scrapes WHERE scrape_id = scrape_id_a) as timestamp, (SELECT config FROM scrapes WHERE scrape_id = scrape_id_a) as config FROM results as a);
```


## Website list:
This scraper was designed to work with the Alexa top websites list, retrieved from http://s3.amazonaws.com/alexa-static/top-1m.csv.zip
<br>A copy of this list, retrieved somewhere in late april 2021, can be found in /libs/top-1m.csv<br>

To shorten this list to the top x websites, this simple bash script can be used: 
```
cat top-1m.csv | cut -d "," -f2 | head -n x > topx.txt
```

## JAP
This scraper was designed to scrape with JAP/AN.ON/Jondonym, a now defunct anonymity network.
It used to be available on https://anonymous-proxy-servers.net/en/software.html but any HTTP proxy can be made to work. 
Just create your own ScraperProfile and SeleniumScraper. 

## Libraries and software used
* [Project Lombok](https://projectlombok.org/) - Used to create getters and setters.
* [Selenium-java](https://www.selenium.dev/) - Used for scraping the webpages.
* Selenium-firefox-driver - Using FireFox with Selenium. - gecko driver included in /libs
* [JSoup](https://jsoup.org/) - Used to parse stored HTML and extract information from it.
* [JImageHash](https://github.com/KilianB/JImageHash) - Used to compare screenshots of webpages to test for similarity.
* Jetbrains UI designer - Used to create the GUI for manual verification.
* [BrowserUp Proxy](https://github.com/browserup/browserup-proxy) - Used to create a slower regular connection.
* [YourKit Java Profiler](https://www.yourkit.com/java/profiler/features/) - Used to trace a memory leak. A personal favorite, great tool to profile an application and find issues, with the academic license being a great deal.
* [uBlock origin](https://github.com/gorhill/uBlock) - (FireFox plugin) Used to reduce bandwidth usage and randomness by removing ads. - extension included in /libs
* [LazyLoadify](https://github.com/gildas-lormeau/LazyLoadify) - (FireFox plugin) Used to reduce bandwidth usage by forcing lazy-loading on all images. - extension included in /libs
* JUnit - Not actually used, its just left in the pom from when there were some basic tests.

#### Licensing
No effort was put into checking whether licensing of used and included libraries are violated. 
Any license violations were by accident, and no harm was intended.
In case of licensing issues, open up an issue or pull request to get it rectified.
This project is licensed under GNU-GPLv3 to allow anyone to use, modify, and (re)distribute freely.