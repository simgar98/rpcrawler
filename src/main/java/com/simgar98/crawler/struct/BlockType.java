/*
 *     RPCrawler
 *     Copyright (C) 2021  Jurgen Mulder
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.simgar98.crawler.struct;

import lombok.Getter;

public enum BlockType {

    NOT_BLOCKED(false),
    IMPRESSIVELY_INDENTICAL(false),
    CAPTCHA(true),
    CAPTCHA_WRONG_SIDE(false),
    CAPTCHA_IDENTICAL_PAGE(false),
    DOM_DIFFERENCE(false),
    CONTENT_LENGTH_DIFFERENCE(false),
    BLOCK_PAGE(true),
    BLOCK_PAGE_EMPTY(true),
    BLOCK_PAGE_WRONG_SIDE(false),
    BLOCK_PAGE_ADBLOCK(false),
    UNKNOWN_DIFFERENT_PAGE(false),
    ERRORED(false),
    ERRORED_NSS(true),
    ERRORED_DNS(false),
    ERRORED_CONNECTION(true),
    SCREENSHOT_DIFFERENCE(false),
    TIMEOUT(false),
    NETWORK_ISSUES(false),
    ;

    @Getter private boolean block;
    BlockType(boolean block) {
        this.block = block;
    }

}