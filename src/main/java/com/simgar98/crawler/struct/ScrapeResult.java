/*
 *     RPCrawler
 *     Copyright (C) 2021  Jurgen Mulder
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.simgar98.crawler.struct;

import com.simgar98.crawler.manager.DatabaseManager;
import com.simgar98.crawler.scrape.SeleniumScraper;
import com.simgar98.crawler.util.DOMUtils;
import com.simgar98.crawler.util.Logger;
import lombok.Getter;
import lombok.Setter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;

import java.io.File;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class ScrapeResult {

    @Getter @Setter private int id;
    @Getter private SeleniumScraper scraper;
    @Getter private String url;
    @Getter private String title;
    @Getter protected long timeTaken;
    @Getter private String content;
    @Getter private String visibleText;
    @Getter private File screenshot;
    @Getter protected int configId;

    private Document parsedDocument;

    public ScrapeResult(SeleniumScraper scraper, String url, String title, long timeTaken, String content, String visibleText, File screenshot) {
        this.id = -1;
        this.scraper = scraper;
        this.url = url;
        this.title = title;
        this.timeTaken = timeTaken;
        this.content = content;
        this.visibleText = visibleText;
        this.screenshot = screenshot;

        if (scraper != null) {
            this.configId = scraper.getConfigId();
        }
    }

    public static class ScrapeResultTimeout extends ScrapeResult {

        public ScrapeResultTimeout(SeleniumScraper scraper, String url, String title, String content, String visibleText, File screenshot) {
            super(scraper, url, title, Long.MAX_VALUE, content, visibleText, screenshot);
        }

    }

    public static class ScrapeResultError extends ScrapeResult {

        public ScrapeResultError(SeleniumScraper scraper, String url, String title) {
            super(scraper, url, title, Long.MAX_VALUE, "Errored", "Errored", null);
        }

    }

    public static class StoredScrapeResult extends ScrapeResult {

        @Getter @Setter private long time;
        @Getter private int id;
        @Getter private WebPage webPage;

        public StoredScrapeResult(int id, WebPage page, String title, long timeTaken, long time, int config, String content, String visibleText, File screenshot) {
            super(null, page.getUrl(), title, timeTaken, content, visibleText, screenshot.exists() ? screenshot : null);
            this.configId = config;
            this.time = time;
            this.id = id;
            this.webPage = page;
        }

        public void setNetworkIssues() {
            this.timeTaken = -2;
            try {
                DatabaseManager.getInstance().updateTimeTaken(this);
            } catch (SQLException e) {
                Logger.log("@REDError updating network issue in db");
                Logger.log(e);
            }
        }
    }

    public Document getDocument() {
        if (parsedDocument == null) {
            parsedDocument = Jsoup.parse(getContent() == null ? "" : getContent());
        }

        return parsedDocument;
    }

    public int countElementsByTag(String tag) {
        Document doc = getDocument();
        return doc.getElementsByTag(tag).size();
    }

    public HashMap<Tag, Integer> countSortedElementsByTags() {
        Document doc = getDocument();
        HashMap<Tag, Integer> map = new HashMap<>();
        for (Element element : doc.getAllElements()) {
            map.put(element.tag(), map.getOrDefault(element.tag(), 0) + 1);
        }

        return map;
    }

    public boolean hasCaptcha() {
        Document doc = getDocument();
        return doc.getAllElements().stream()
                .anyMatch(DOMUtils::isCaptchaElement);
    }

    public boolean isError() {
        if (this instanceof ScrapeResultError) {
            return true;
        }

        if (getScreenshot() == null || getScreenshot().getName().startsWith("about:")) {
            return true;
        }

        return getContent().equalsIgnoreCase("Errored");
    }

    public boolean isErrorDNS() {
        if (!isError()) {
            return false;
        }

        if (getTitle() == null) {
            return false;
        }

        return getTitle().equalsIgnoreCase("dnsNotFound");
    }

    public boolean isErrorConnection() {
        if (!isError()) {
            return false;
        }

        if (getTitle() == null) {
            return false;
        }

        return getTitle().equalsIgnoreCase("connectionFailure")
                || getTitle().equalsIgnoreCase("nssFailure")
                || getTitle().contains("redirectLoop")
                || getTitle().equalsIgnoreCase("proxyConnectFailure")
                || getTitle().toLowerCase().contains("cannot contact");
    }

    public boolean isTimedOut() {
        if (this instanceof ScrapeResultTimeout) {
            return true;
        }

        // Safe to say that more than 2147483 seconds is a timeout, innit? -1 is db
        // 2147483 s = ~25 days ayyy
        return getTimeTaken() >= Integer.MAX_VALUE || getTimeTaken() == -1;
    }

    public boolean isNetworkIssues() {
        return getTimeTaken() == -2;
    }

    public boolean isAdBlocked() {
        if (getScreenshot() != null && getScreenshot().getName().startsWith("moz-extension:")) {
            return true;
        }

        return getFullText().contains("Disable strict blocking for");
    }

    public Map<BlockKeyWord, Integer> countKeywords() {
        Map<BlockKeyWord, Integer> map = new HashMap<>();

        String text = getFullText().toLowerCase();
        for (BlockKeyWord key : BlockKeyWord.values()) {
            for (String sign : key.getSigns()) {
                if (text.contains(sign)) {
                    map.put(key, map.getOrDefault(key, 0) + 1);
                }
            }
        }

        return map;
    }

    public String getFullText() {
        if (getVisibleText() != null) {
            return getVisibleText();
        }

        Document doc = getDocument();
        if (doc == null || doc.body() == null) {
            return "";
        }

        return doc.body().text().toLowerCase();
    }

}
