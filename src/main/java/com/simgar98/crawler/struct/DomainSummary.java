/*
 *     RPCrawler
 *     Copyright (C) 2021  Jurgen Mulder
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.simgar98.crawler.struct;

import com.simgar98.crawler.Constants;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

public class DomainSummary {

    @Getter private String domain;
    private List<StoredComparison> comparisons;
    @Getter private int comparisonCount;
    @Getter private int blocks;
    @Getter private double blockPercentage;
    @Getter private DomainVerdict verdict;

    public DomainSummary(String domain) {
        this.domain = domain;
        this.comparisons = new ArrayList<>();
    }

    public void add(StoredComparison comparison) {
        this.comparisons.add(comparison);
    }

    public void summarize() {
        this.comparisonCount = (int) this.comparisons.stream().filter(comp -> comp.getVerdict() != BlockType.NETWORK_ISSUES).count();
        this.blocks = (int) this.comparisons.stream().filter(comp -> comp.getVerdict().isBlock()).count();
        this.blockPercentage = (double) getBlocks() / (double) getComparisonCount();
        if (getBlockPercentage() >= Constants.getSUMMARY_WORST_CATEGORY_LIMIT()) {
            this.verdict = DomainVerdict.FULL_BLOCKING;
            return;
        }

        if (getBlocks() >= 1) {
            this.verdict = DomainVerdict.SOME_BLOCKING;
            return;
        }

        this.verdict = DomainVerdict.NO_BLOCKING;
    }

}
