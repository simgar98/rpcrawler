/*
 *     RPCrawler
 *     Copyright (C) 2021  Jurgen Mulder
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.simgar98.crawler.struct;

import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/*
Yeah, the naming of this sucks related to ScrapeComparison...
 */
public class StoredComparison {

    @Getter private int scrapeIdA;
    @Getter private int scrapeIdB;
    @Getter private int experiment;
    @Getter private WebPage page;
    @Getter private BlockType verdict;
    @Getter private List<BlockType> blockTypes;

    public StoredComparison(int scrapeIdA, int scrapeIdB, int config, BlockType verdict, WebPage page, List<BlockType> blocks) {
        this.scrapeIdA = scrapeIdA;
        this.scrapeIdB = scrapeIdB;
        this.experiment = config;
        this.verdict = verdict;
        this.page = page;
        this.blockTypes = blocks;
    }

    public StoredComparison(int scrapeIdA, int scrapeIdB, int experiment, BlockType verdict, WebPage page, String rawBlocks) {
        this(scrapeIdA, scrapeIdB, experiment, verdict, page,
                Arrays.stream(rawBlocks.split(","))
                        .filter(raw -> raw.length() > 0)
                        .map(BlockType::valueOf)
                        .collect(Collectors.toList())
        );
    }

}
