/*
 *     RPCrawler
 *     Copyright (C) 2021  Jurgen Mulder
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.simgar98.crawler.struct;

import com.github.kilianB.hash.Hash;
import com.github.kilianB.hashAlgorithms.HashingAlgorithm;
import com.github.kilianB.hashAlgorithms.PerceptiveHash;
import com.github.romankh3.image.comparison.ImageComparison;
import com.github.romankh3.image.comparison.ImageComparisonUtil;
import com.github.romankh3.image.comparison.model.ImageComparisonResult;
import com.simgar98.crawler.Constants;
import com.simgar98.crawler.util.Logger;
import lombok.Getter;
import lombok.Setter;
import org.jsoup.parser.Tag;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

public class ScrapeComparison {

    @Getter private WebPage webPage;
    // A = the control
    @Getter private ScrapeResult a;
    // B = the experiment (or another control I suppose)
    @Getter private ScrapeResult b;

    @Getter private boolean captchaDifference;
    @Getter private boolean timeoutDifference;
    @Getter private double simpleDomSimilarity;
    @Getter private double domSizeSimilarity;
    @Getter private double textLengthSimilarity;
    @Getter private boolean errorDifference;
    @Getter private double screenshotDifference;
    @Getter private File screenshotCompared;
    @Getter private Map<BlockKeyWord, Integer> keywords;
    @Getter private List<BlockType> blocks;
    @Getter @Setter private BlockType finalVerdict;

    public ScrapeComparison(WebPage webPage, ScrapeResult a, ScrapeResult b) {
        this.webPage = webPage;
        // Ensure control and experiment ordering, makes our lives easier
        if (b.getConfigId() == 0 && a.getConfigId() != 0) {
            ScrapeResult t = a;
            a = b;
            b = t;
        }

        this.a = a;
        a.getDocument();
        this.b = b;
        b.getDocument();

        summarize();
    }

    private void summarize() {
        blocks = new ArrayList<>();
        checkTimeout();
        checkDOMSimilarity();
        checkDOMSize();
        checkEmptyPage();
        checkError();
        checkScreenshotsHash();
        checkKeywords();
        checkCaptchas();
        checkAdblock();

        checkFinalVerdict();
        Logger.log("@GREENReached verdict on " + getName() + ": @YELLOW" + getFinalVerdict());
    }

    private void checkFinalVerdict() {
        if (blocks.contains(BlockType.NETWORK_ISSUES)) {
            this.finalVerdict = BlockType.NETWORK_ISSUES;
            return;
        }

        if (blocks.contains(BlockType.BLOCK_PAGE_ADBLOCK)) {
            this.finalVerdict = BlockType.BLOCK_PAGE_ADBLOCK;
            return;
        }

        if (blocks.contains(BlockType.CAPTCHA)) {
            this.finalVerdict = BlockType.CAPTCHA;
            return;
        }

        if (blocks.contains(BlockType.ERRORED)) {
            this.finalVerdict = BlockType.ERRORED;
            if (blocks.contains(BlockType.ERRORED_CONNECTION)) {
                this.finalVerdict = BlockType.ERRORED_CONNECTION;
                return;
            }

            if (blocks.contains(BlockType.ERRORED_NSS)) {
                this.finalVerdict = BlockType.ERRORED_NSS;
                return;
            }

            if (blocks.contains(BlockType.ERRORED_DNS)) {
                this.finalVerdict = BlockType.ERRORED_DNS;
                return;
            }

            if (blocks.contains(BlockType.TIMEOUT)) {
                this.finalVerdict = BlockType.TIMEOUT;
                return;
            }
            return;
        }

        if (blocks.contains(BlockType.SCREENSHOT_DIFFERENCE) && blocks.contains(BlockType.BLOCK_PAGE)) {
            this.finalVerdict = BlockType.BLOCK_PAGE;
            return;
        }

        if (blocks.contains(BlockType.TIMEOUT) &&
                (blocks.contains(BlockType.DOM_DIFFERENCE) || blocks.contains(BlockType.SCREENSHOT_DIFFERENCE))) {
            this.finalVerdict = BlockType.TIMEOUT;
            return;
        }

        if (blocks.contains(BlockType.SCREENSHOT_DIFFERENCE) && blocks.contains(BlockType.BLOCK_PAGE_EMPTY)) {
            this.finalVerdict = BlockType.BLOCK_PAGE_EMPTY;
            return;
        }

        if (screenshotDifference >= 0.01 && blocks.stream().filter(type -> type == BlockType.DOM_DIFFERENCE
                || type == BlockType.CONTENT_LENGTH_DIFFERENCE || type == BlockType.SCREENSHOT_DIFFERENCE).count() >= 2) {
            this.finalVerdict = BlockType.UNKNOWN_DIFFERENT_PAGE;
            return;
        }

        if ((domSizeSimilarity >= 0.999 && simpleDomSimilarity >= 0.999 && getScreenshotDifference() <= 0.001)
                || getScreenshotDifference() <= 0.000001) {
            this.finalVerdict = BlockType.IMPRESSIVELY_INDENTICAL;
            return;
        }

        this.finalVerdict = BlockType.NOT_BLOCKED;
    }

    private void checkTimeout() {
        timeoutDifference = a.isTimedOut() != b.isTimedOut();

        if (isTimeoutDifference()) {
            blocks.add(BlockType.TIMEOUT);
        }

        if (getA().isNetworkIssues() || getB().isNetworkIssues()) {
            blocks.add(BlockType.NETWORK_ISSUES);
        }
    }

    private void checkError() {
        errorDifference = getA().isError() != getB().isError();

        if (isErrorDifference()) {
            blocks.add(BlockType.ERRORED);

            if (getA().isErrorDNS() || getB().isErrorDNS()) {
                blocks.add(BlockType.ERRORED_DNS);
            }

            if (getA().isErrorConnection() || getB().isErrorConnection()) {
                blocks.add(BlockType.ERRORED_CONNECTION);
            }
        }
    }

    private void checkDOMSize() {
        int aSize = getA().getContent().length();
        int bSize = getB().getContent().length();
        domSizeSimilarity = 1.0 - ((double) Math.abs(aSize - bSize)) / ((double) Math.max(aSize, bSize));

        if (getDomSizeSimilarity() <= Constants.getDOM_LENGTH_CUTOFF()) {
            blocks.add(BlockType.CONTENT_LENGTH_DIFFERENCE);
        }
    }

    private void checkDOMSimilarity() {
        HashMap<Tag, Integer> aCount = getA().countSortedElementsByTags();
        HashMap<Tag, Integer> bCount = getB().countSortedElementsByTags();
        int diff = 0;
        int total = 0;
        Set<Tag> tags = new HashSet<>();
        tags.addAll(aCount.keySet());
        tags.addAll(bCount.keySet());

        for (Tag tag : tags) {
            double aAmnt = aCount.getOrDefault(tag, 0);
            double bAmnt = bCount.getOrDefault(tag, 0);
            diff += Math.abs(aAmnt - bAmnt);
            total += Math.max(aAmnt, bAmnt);
        }

        simpleDomSimilarity = 1.0 - ((double) diff / (double) total);

        if (getSimpleDomSimilarity() <= Constants.getDOM_SIMILARITY_SIMPLE_CUTOFF()) {
            blocks.add(BlockType.DOM_DIFFERENCE);
            Logger.log("Dom Difference based on " + getSimpleDomSimilarity() + " on " + getName());
        }
    }

    /*
    This check can falseflag easily on pages that are not finished loading...
     */
    private void checkEmptyPage() {
        int aSize = getA().getFullText().length();
        int bSize = getB().getFullText().length();
        textLengthSimilarity = 1.0 - ((double) Math.abs(aSize - bSize)) / ((double) Math.max(aSize, bSize));

        if (isTimeoutDifference()) {
            return;
        }

        if (simpleDomSimilarity > Constants.getEMPTY_MAX_DOM_SIMILARITY()) {
            return;
        }

        if (textLengthSimilarity <= Constants.getTEXT_LENGTH_CUTOFF() && bSize < aSize) {
            blocks.add(BlockType.BLOCK_PAGE_EMPTY);
        }
    }

    private void checkCaptchas() {
        boolean aHas = getA().hasCaptcha();
        boolean bHas = getB().hasCaptcha();
        captchaDifference = aHas != bHas;
        if (!isCaptchaDifference()) {
            return;
        }

        // Avoiding false positives with the control having a captcha
        // semi-recently added that A is always control, B is always experiment
        if (aHas && !bHas) {
            blocks.add(BlockType.CAPTCHA_WRONG_SIDE);
            return;
        }

        // Avoiding false positives with hidden captchas somewhere down or sth
        // If a captcha is down super far somewhere, it's not really a captcha page is it?
        if (getDomSizeSimilarity() >= 0.95 && getSimpleDomSimilarity() >= 0.95 && getScreenshotDifference() <= 0.01) {
            blocks.add(BlockType.CAPTCHA_IDENTICAL_PAGE);
            return;
        }

        blocks.add(BlockType.CAPTCHA);
    }

    private void checkScreenshotsHash() {
        if (isMissingScreenshot()) {
            Logger.log("@REDNo screenshot found for hash of " + getName());
            return;
        }

        HashingAlgorithm algo = new PerceptiveHash(64);
        try {
            Hash hashA = algo.hash(getA().getScreenshot());
            Hash hashB = algo.hash(getB().getScreenshot());

            screenshotDifference = hashA.normalizedHammingDistance(hashB);
        } catch (Exception e) {
            Logger.log("@REDError comparing hashes of " + getName());
            Logger.log(e);
        }

        if (getScreenshotDifference() >= Constants.getIMAGE_DIFFERENCE_HASH_LIMIT()) {
            blocks.add(BlockType.SCREENSHOT_DIFFERENCE);
        }
    }

    private void checkScreenshots() {
        if (isMissingScreenshot()) {
            Logger.log("@REDNo screenshot found for " + getName());
            return;
        }

        if (domSizeSimilarity >= 0.999 && simpleDomSimilarity >= 0.999) {
            Logger.log("@GREENSkipping screenshot check for " + getName() + " as the content is way too similar");
            screenshotDifference = -1.0;
            return;
        }

        BufferedImage aImage = ImageComparisonUtil.readImageFromResources(a.getScreenshot().getAbsolutePath());
        BufferedImage bImage = ImageComparisonUtil.readImageFromResources(b.getScreenshot().getAbsolutePath());
        if (bImage.getHeight() != aImage.getHeight() || bImage.getWidth() != aImage.getHeight()) {
            bImage = ImageComparisonUtil.resize(bImage, aImage.getWidth(), aImage.getHeight()); // Ensure same dimensions
        }

        ImageComparisonResult result = new ImageComparison(aImage, bImage)
                .setAllowingPercentOfDifferentPixels(0.05) // Allow some differences, because text and images can differ
                .setPixelToleranceLevel(0.04) // Percentage difference per pixel, allow some recoloring and stuff
                .setThreshold(15) // Max distance between non-matching pixels
                .setMinimalRectangleSize(6) // Minimal size of a rectangle
                .compareImages();

        screenshotDifference = result.getDifferencePercent()/100.0;
        if (getScreenshotDifference() >= Constants.getIMAGE_DIFFERENCE_PERCENT_LIMIT()) {
            blocks.add(BlockType.SCREENSHOT_DIFFERENCE);
            File save = getComparisonImageFileName();
            ImageComparisonUtil.saveImage(save, result.getResult());
            screenshotCompared = save;
        }
    }

    private boolean isMissingScreenshot() {
        return getA().getScreenshot() == null || getB().getScreenshot() == null
                || !getA().getScreenshot().exists() || !getB().getScreenshot().exists();
    }

    private void checkAdblock() {
        if (getA().isAdBlocked() || getB().isAdBlocked()) {
            blocks.add(BlockType.BLOCK_PAGE_ADBLOCK);
        }
    }

    private File getComparisonImageFileName() {
        File target = null;
        int id = 0;
        while (target == null || target.exists()) {
            target = new File("logs/screenshotdifference/" + getA().getId() + "-" + getB().getId() + "-" + id + ".png");
            if (target.getParentFile().mkdirs()) {
                Logger.log("Created a folder for screenshot differences");
            }
            id++;
        }

        return target;
    }

    private void checkKeywords() {
        keywords = new HashMap<>();

        Map<BlockKeyWord, Integer> aWords = getA().countKeywords();
        Map<BlockKeyWord, Integer> bWords = getB().countKeywords();
        int total = 0;
        int aTotal = 0;
        int bTotal = 0;

        for (BlockKeyWord keyword : BlockKeyWord.values()) {
            int aCount = aWords.getOrDefault(keyword, 0);
            aTotal += aCount;
            int bCount = bWords.getOrDefault(keyword, 0);
            bTotal += bCount;

            int diff = Math.abs(aCount - bCount);
            if (diff == 0) {
                continue;
            }

            total += diff;
            keywords.put(keyword, diff);
        }

        Logger.log(getName() + " BlockKeyWords: " + aTotal + "/" + bTotal);
        Logger.log(getName() + " Block keywords A: " + mapToString(aWords));
        Logger.log(getName() + " Block keywords B: " + mapToString(bWords));
        if (total >= Constants.getDOM_MAX_KEYWORD_DIFF()) {
            Logger.log("Flagging as block page " + getName());
            if (bTotal >= aTotal) {
                blocks.add(BlockType.BLOCK_PAGE);
            } else {
                blocks.add(BlockType.BLOCK_PAGE_WRONG_SIDE);
            }
        }
    }

    private String mapToString(Map<BlockKeyWord, Integer> map) {
        return map.entrySet().stream()
                .map(e -> e.getKey().toString() + ":" + e.getValue())
                .collect(Collectors.joining(", "));
    }

    public String getName() {
        return a.getId() + "vs" + b.getId();
    }

}
