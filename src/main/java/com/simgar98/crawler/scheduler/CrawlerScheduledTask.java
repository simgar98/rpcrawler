/*
 *     RPCrawler
 *     Copyright (C) 2021  Jurgen Mulder
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.simgar98.crawler.scheduler;

import com.simgar98.crawler.util.Logger;
import lombok.Getter;

public class CrawlerScheduledTask implements Runnable {

    @Getter private Long scheduleTime;
    private Runnable runnable;
    @Getter private int id;

    public CrawlerScheduledTask(Long scheduleTime, Runnable runnable, int id) {
        this.scheduleTime = scheduleTime;
        this.runnable = runnable;
        this.id = id;
    }

    @Override
    public void run() {
        try {
            runnable.run();
        } catch (Exception e) {
            Logger.log("Error in scheduled task " + getId());
            e.printStackTrace();
        }
    }

}