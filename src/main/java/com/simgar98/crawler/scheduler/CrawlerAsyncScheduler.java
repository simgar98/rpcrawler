/*
 *     RPCrawler
 *     Copyright (C) 2021  Jurgen Mulder
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.simgar98.crawler.scheduler;

import lombok.Getter;

import java.util.Objects;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class CrawlerAsyncScheduler {

    private static CrawlerAsyncScheduler instance = new CrawlerAsyncScheduler();
    private PriorityBlockingQueue<CrawlerScheduledTask> tasks;
    private PriorityBlockingQueue<CrawlerScheduledTask> tasksToAdd;
    private PriorityBlockingQueue<CrawlerRepeatingTask> repeatingTasks;
    private PriorityBlockingQueue<CrawlerRepeatingTask> repeatingTasksToAdd;
    private Thread schedulerThread;
    @Getter private boolean running;
    @Getter private ExecutorService pool;
    @Getter private AtomicInteger nextTaskId;

    private CrawlerAsyncScheduler() {
        instance = this;

        log("Launching scheduler...");
        running = true;
        nextTaskId = new AtomicInteger(0);
        tasks = new PriorityBlockingQueue<>(100, this::compareTasks);
        tasksToAdd = new PriorityBlockingQueue<>(10, this::compareTasks);
        repeatingTasks = new PriorityBlockingQueue<>(100, this::compareRepeating);
        repeatingTasksToAdd = new PriorityBlockingQueue<>(10, this::compareRepeating);

        pool = new ThreadPoolExecutor(10, 6942, 4L, TimeUnit.MINUTES, new LinkedBlockingQueue<>());

        schedulerThread = new Thread(this::checkSchedule);
        schedulerThread.setName("ElevateAsyncScheduler");
        schedulerThread.start();
        log("... Scheduler launched!");
    }

    public static CrawlerScheduledTask submit(Runnable runnable, Long delay) {
        CrawlerScheduledTask task = new CrawlerScheduledTask(System.currentTimeMillis() + delay, runnable, instance.nextTaskId.getAndIncrement());
        if (delay <= 0) {
            instance.getPool().submit(task);
            return task;
        }

        instance.tasksToAdd.add(task);
        return task;
    }

    public static void submitRepeating(Runnable runnable, Long interval) {
        submitRepeating(runnable, interval, true);
    }

    public static CrawlerRepeatingTask submitRepeating(Runnable runnable, Long interval, boolean ignoreRunning) {
        CrawlerRepeatingTask task = new CrawlerRepeatingTask(interval, runnable, ignoreRunning, instance.nextTaskId.getAndIncrement());
        instance.repeatingTasksToAdd.add(task);
        return task;
    }

    private void checkSchedule() {
        while (isRunning()) {
            if (getPool() == null) {
                try {
                    log("No thread pool! Sleeping!");
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                continue;
            }

            if (nextTaskId.get() >= 500000000) {
                log("Reset the task counter! We passed " + nextTaskId.get());
                nextTaskId.set(0);
            }

            try {
                PriorityBlockingQueue<CrawlerScheduledTask> tasksToAddCopy = tasksToAdd;
                while (!tasksToAddCopy.isEmpty()) {
                    tasks.add(tasksToAddCopy.remove());
                }

                PriorityBlockingQueue<CrawlerRepeatingTask> repeatingTasksToAddCopy = repeatingTasksToAdd;
                while (!repeatingTasksToAddCopy.isEmpty()) {
                    repeatingTasks.add(repeatingTasksToAddCopy.remove());
                }

                while (tasks.peek() != null && tasks.peek().getScheduleTime() <= System.currentTimeMillis()) {
                    CrawlerScheduledTask task = tasks.remove();
                    getPool().submit(task);
                }

                repeatingTasks.stream()
                        .filter(Objects::nonNull)
                        .filter(CrawlerRepeatingTask::shouldSchedule)
                        .forEach(task -> {
                            task.setLastRan(System.currentTimeMillis());
                            getPool().submit(task);
                        });
                repeatingTasks.removeIf(Objects::isNull);

                Thread.sleep(10);
            } catch (Exception e) {
                log("Exception in scheduler");
                e.printStackTrace();
            }
        }
    }

    public int countTasks() {
        return tasks.size();
    }

    public int countRepeatingTasks() {
        return repeatingTasks.size();
    }

    public void disable() {
        running = false;
        pool.shutdown();
    }

    private int compareTasks(CrawlerScheduledTask a, CrawlerScheduledTask b) {
        return (int) (a.getScheduleTime() - b.getScheduleTime());
    }

    private int compareRepeating(CrawlerRepeatingTask a, CrawlerRepeatingTask b) {
        return a.getId() - b.getId();
    }

    public static CrawlerAsyncScheduler getInstance() {
        if (instance == null) {
            instance = new CrawlerAsyncScheduler();
        }

        return instance;
    }

    protected static void log(String msg) {
        System.out.println("[FAS] " + msg);
    }

}
