/*
 *     RPCrawler
 *     Copyright (C) 2021  Jurgen Mulder
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.simgar98.crawler.scheduler;

import com.simgar98.crawler.util.Logger;
import lombok.Getter;
import lombok.Setter;

public class CrawlerRepeatingTask implements Runnable {

    @Getter private Long interval;
    @Getter @Setter private Long lastRan;
    @Getter @Setter private Long lastFinished;
    private Runnable runnable;
    @Getter private boolean running;
    @Getter @Setter private boolean ignoreRunning;
    @Getter private int id;

    public CrawlerRepeatingTask(Long interval, Runnable runnable, boolean ignoreRunning, int id) {
        this.interval = interval;
        this.runnable = runnable;
        this.lastRan = System.currentTimeMillis();
        this.lastFinished = System.currentTimeMillis();
        this.running = false;
        this.ignoreRunning = ignoreRunning;
        this.id = id;
    }

    public boolean shouldSchedule() {
        if (running && !isIgnoreRunning()) {
            return false;
        }

        if (!isIgnoreRunning()) {
            return System.currentTimeMillis() >= getLastFinished() + getInterval();
        }

        return System.currentTimeMillis() >= getLastRan() + getInterval();
    }

    @Override
    public void run() {
        this.running = true;
        this.lastRan = System.currentTimeMillis();
        try {
            runnable.run();
        } catch (Exception e) {
            Logger.log("Error in repeating task " + getId());
            e.printStackTrace();
        }
        this.running = false;
        this.lastFinished = System.currentTimeMillis();
    }

}