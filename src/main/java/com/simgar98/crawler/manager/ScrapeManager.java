/*
 *     RPCrawler
 *     Copyright (C) 2021  Jurgen Mulder
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.simgar98.crawler.manager;

import com.simgar98.crawler.Constants;
import com.simgar98.crawler.scheduler.CrawlerAsyncScheduler;
import com.simgar98.crawler.scrape.*;
import com.simgar98.crawler.struct.ScrapeResult;
import com.simgar98.crawler.util.LinkUtils;
import com.simgar98.crawler.util.Logger;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class ScrapeManager {

    private static ScrapeManager instance;

    private ScrapeManager() {
        instance = this;
    }

    public void testScraperFunctionalityList() {
        List<String> urls = Arrays.asList("https://aap.ovh/sitestuff/bigimage",
                "https://aap.ovh/",
//                "https://aap.ovh/infiniteload",
                "https://homedepot.com/",
                "https://sticky.best/",
                "https://ipchicken.com/",
                "https://www.amazon.ca/Samsung-QN85A-QN55QN85AAFXZC-Canada-Version/dp/B08XK4Q15Z/",
                "https://aap.ovh/sitestuff/bigimage");
        compareScrapeList(urls);
    }

    public void compareScrapeList(List<String> urls) {
        FirefoxScraper regularScraper = new FirefoxScraper();
        FirefoxScraper testScraper;
        try {
            testScraper = generateTestScraper(Constants.getEXPERIMENT_PROFILE());
        } catch (Exception e) {
            System.out.println("AAAAAA");
            Logger.log("Could not set up test scraper");
            Logger.log(e);
            return;
        }

        long start = System.currentTimeMillis();
        for (String url : urls) {
            url = LinkUtils.fixUrl(url);
            comparedScrape(url, regularScraper, testScraper);
        }

        long timeSpent = System.currentTimeMillis() - start;
        Logger.log("@GREENFinished scraping " + urls.size() + " URLS in " + timeSpent + "ms (" + (timeSpent/1000) + "s)");

        // Closes the windows/scrapers
        CrawlerAsyncScheduler.submit(() -> {
            regularScraper.quit();
            testScraper.quit();
        }, 3000L);
    }

    private FirefoxScraper generateTestScraper(ScraperProfile profile) {
        // This could be done with having the class in the enum and doing some reflection magic
        // But this is a lot easier!
        Logger.log("Going to generate test scraper of " + profile);
        switch (profile) {
            case FIREFOX_REGULAR:
                return new FirefoxScraper(3);
            case FIREFOX_JAP:
                return new FirefoxScraperJAP();
            case FIREFOX_REGULAR_SLOW:
                return new FirefoxScraperSlow();
//                return new FirefoxScraper(4);
        }

        throw new IllegalArgumentException("Passed unimplemented ScrapeProfile: " + profile);
    }

    public List<String> scrapeLinks(List<String> urls, int amountPerDomain, int amountOfDomains) {
        List<String> links = new ArrayList<>();
        AtomicInteger domains = new AtomicInteger();
        int launchedCrawlers = Math.min(amountOfDomains, 7);
        AtomicInteger finishedCrawlers = new AtomicInteger();
        Logger.log("@YELLOWStarting " + launchedCrawlers + " crawlers!");

        List<List<String>> dividedUrls = new ArrayList<>();
        // This is an attempt at splitting the list of URLS into lists with an equal ranking order,
        // such that we dont get a weird mix of 0-3, 10-13, 20-23, 30-33, 40-43 when doing top 50 for example.
        // I have no idea how to put this into proper words...
        for (int i = 0; i < launchedCrawlers; i++) {
            List<String> partList = new ArrayList<>();
            for (int pi = i; pi < urls.size(); pi += launchedCrawlers) {
                partList.add(urls.get(pi));
            }
            Logger.log("@YELLOWList " + i + " is " + partList.size() + " long, start: " +
                    partList.stream().limit(3).collect(Collectors.joining(", ")));
            dividedUrls.add(partList);
        }

        Logger.log("@YELLOWDivided urls into " + dividedUrls.size() + " lists");
        Integer[] currentIndex = new Integer[launchedCrawlers];

        for (int crawlerIndex = 0; crawlerIndex < launchedCrawlers; crawlerIndex++) {
            int finalCrawlerIndex = crawlerIndex;
            CrawlerAsyncScheduler.submit(() -> {
                Random random = new Random();
                FirefoxScraper scraper = new FirefoxScraper();
                int i = 0;
                for (String url : dividedUrls.get(finalCrawlerIndex)) {
                    Logger.log("@YELLOWCrawler " + finalCrawlerIndex + " is starting on index " + i + " - " + domains.get() + "/" + amountOfDomains);
                    currentIndex[finalCrawlerIndex] = i;
                    while (Arrays.stream(currentIndex).filter(Objects::nonNull).min(Integer::compareTo).orElse(0) < i - launchedCrawlers) {
                        if (domains.get() >= amountOfDomains) {
                            break;
                        }

                        try {
                            Logger.log("@YELLOWCrawler " + finalCrawlerIndex + " is pausing for the rest to catch up... " + i, true);
                            Thread.sleep(1000 + random.nextInt(3000));
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                    i++;
                    if (LinkUtils.isBlacklistedDomain(url)) {
                        Logger.log("@YELLOWIgnoring URL " + url + " as it is blacklisted!");
                        continue;
                    }

                    url = LinkUtils.fixUrl(url);
                    List<String> domainLinks = scraper.scrapeRandomLinks(url, amountPerDomain);
                    if (domainLinks == null) {
                        Logger.log("@REDNot scraping/including links of domain " + url);
                        continue;
                    }

                    if (domains.get() >= amountOfDomains) {
                        break;
                    }

                    links.add(url);
                    links.addAll(domainLinks);
                    if (domains.addAndGet(1) >= amountOfDomains) {
                        break;
                    }
                }

                Logger.log("@YELLOWFinished link crawler " + finalCrawlerIndex);
                finishedCrawlers.incrementAndGet();
                CrawlerAsyncScheduler.submit(scraper::quit, 500L);
            }, 0L);
        }

        while (finishedCrawlers.get() < launchedCrawlers) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        Logger.log("@GREENFound all available links! Domains: " + domains.get() + "/" + amountOfDomains
                + " Links: " + links.size() + "/" + (amountOfDomains * (amountPerDomain + 1)));
        return links;
    }

    private void comparedScrape(String url, SeleniumScraper... scrapers) {
        AtomicInteger loaded = new AtomicInteger();
        List<ScrapeResult> results = new ArrayList<>(scrapers.length);

        for (SeleniumScraper scraper : scrapers) {
            CrawlerAsyncScheduler.submit(() -> {
                ScrapeResult result = scraper.scrapePageSafe(url);
                results.add(result);
                loaded.getAndIncrement();
            }, 0L);
        }

        while (loaded.get() < scrapers.length) {
            try {
                Thread.sleep(100L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        CrawlerAsyncScheduler.submit(() -> DatabaseManager.getInstance().storeScrapeResults(results), 0L);

        Logger.log("@GREENFinished complete scrape of " + url + " - " + results.size());
    }

    public static ScrapeManager getInstance() {
        if (instance == null) {
            instance = new ScrapeManager();
        }

        return instance;
    }

}
