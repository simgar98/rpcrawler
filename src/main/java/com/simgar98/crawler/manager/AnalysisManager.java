/*
 *     RPCrawler
 *     Copyright (C) 2021  Jurgen Mulder
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.simgar98.crawler.manager;

import com.simgar98.crawler.Constants;
import com.simgar98.crawler.scheduler.CrawlerAsyncScheduler;
import com.simgar98.crawler.struct.*;
import com.simgar98.crawler.util.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.*;
import java.util.function.ToLongFunction;
import java.util.stream.Collectors;

public class AnalysisManager {

    private static AnalysisManager instance;
    private List<ScrapeComparison> completedComparisons;

    private AnalysisManager() {
        completedComparisons = Collections.synchronizedList(new ArrayList<>());
    }

    public ScrapeComparison getComparison(int id) {
        Optional<ScrapeComparison> cached = completedComparisons.stream()
                .filter(comp -> (comp.getA().getId() == id || comp.getB().getId() == id))
                .findAny();
        if (cached.isPresent()) {
            return cached.get();
        }

        Logger.log("@YELLOWFinding page of scrape id " + id + " (uncached)");
        try {
            WebPage page = DatabaseManager.getInstance().getWebPageOfScrape(id);
            Logger.log("@YELLOWRe-doing comparison of " + page.getId() + "//" + page.getUrl());
            performAnalysis(page);
        } catch (SQLException e) {
            Logger.log("Error finding page of scrape " + id);
            Logger.log(e);
        }

        Logger.log("Finished comparison, re-getting");
        return getComparison(id);
    }

    public void analyzeAll() {
        try {
            List<WebPage> pages = DatabaseManager.getInstance().getPages("");
            Logger.log("@YELLOWFound " + pages.size() + " webpages in total! Processing this might take a while");
            Logger.log("@YELLOWMaybe go make a cup of tea?");
            int threads = Constants.getANALYSIS_THREADS();
            int perThread = (int) Math.ceil(pages.size() / (double) threads);
            for (int i = 0; i < threads; i++) {
                final int threadId = i;
                final List<WebPage> threadPages = pages.subList(i * perThread, Math.min((i + 1) * perThread, pages.size()));
                CrawlerAsyncScheduler.submit(() -> {
                    Logger.log("Thread " + threadId + " is going to do " + threadPages.size() + " pages!");
                    for (WebPage page : threadPages) {
                        Logger.log("Starting comparison thread " + threadId + " on " + page.getId());
                        try {
                            performAnalysis(page);
                        } catch (Exception e) {
                            Logger.log("@REDException analyzing page " + page.getUrl());
                            Logger.log(e);
                        }

                        completedComparisons.clear();
                        Logger.log("Comparison thread " + threadId + " finished " + page.getId());
                    }

                    Logger.log("Comparison thread " + threadId + " thinks there is no more work!");
                }, i * 50L);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void performAnalysis(String domain) {
        try {
            List<WebPage> pages = DatabaseManager.getInstance().getPages(domain);
            Logger.log("@YELLOWFound " + pages.size() + " webpages for domain " + domain);
            for (WebPage page : pages) {
                try {
                    performAnalysis(page);
                } catch (Exception e) {
                    Logger.log("@REDException analyzing page " + page.getUrl());
                    Logger.log(e);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void performAnalysis(WebPage page) throws SQLException {
        Logger.log("Analyzing page " + page.getId() + " with URL " + page.getUrl());
        List<ScrapeResult.StoredScrapeResult> scrapes = DatabaseManager.getInstance().getResults(page);
        HashMap<Long, List<ScrapeResult.StoredScrapeResult>> timestamped = new HashMap<>();
        for (ScrapeResult.StoredScrapeResult scrape : scrapes) {
            List<ScrapeResult.StoredScrapeResult> list = timestamped.computeIfAbsent(scrape.getTime(), (t) -> new ArrayList<>());
            list.add(scrape);
        }

        Logger.log("Page " + page.getId() + " subdidived in " + timestamped.size() + " moments (" + scrapes.size() + " total scrapes)");
        for (List<ScrapeResult.StoredScrapeResult> list : timestamped.values()) {
            if (list.size() < 2) {
                Logger.log("@REDPage " + page.getId() + " has at least one timestamp with only one result!");
            }

            List<ScrapeComparison> comparisons = new ArrayList<>();
            for (ScrapeResult.StoredScrapeResult a : list) {
                for (ScrapeResult.StoredScrapeResult b : list) {
                    // a >= b, so we dont get a-b and b-a ;)
                    if (a.getId() >= b.getId() || a.getConfigId() == b.getConfigId()) {
                        continue;
                    }

                    ScrapeComparison comp = new ScrapeComparison(page, a, b);
                    comparisons.add(comp);
                    if (comp.getBlocks().size() <= 0) {
                        continue;
                    }

                    Logger.log("@REDBlocks on " + page.getId() + ": " + a.getId() + "vs" + b.getId() + ": " + comp.getBlocks().stream()
                            .map(Enum::toString).collect(Collectors.joining(", ")));
                    Logger.log("Files to compare: " + a.getScreenshot() + " vs " + b.getScreenshot());
                }
            }

            try {
                Logger.log("Going to save comparisons: " + comparisons.stream().map(ScrapeComparison::getName).collect(Collectors.joining(", ")));
                DatabaseManager.getInstance().storeComparisons(comparisons);
                completedComparisons.addAll(comparisons);
            } catch (SQLException e) {
                Logger.log("@REDError saving comparisons");
                Logger.log(e);
            }
        }
    }

    public void analyzeNetworkIssues() throws SQLException {
        Logger.log("@GREENStarting network outage/issue scan... ");
        List<ScrapeResult.StoredScrapeResult> allResults = getAllResults(ScrapeResult.StoredScrapeResult::getTime);

        Map<Integer, List<ScrapeResult.StoredScrapeResult>> byExperiment = new HashMap<>();
        allResults.forEach(result -> byExperiment.computeIfAbsent(result.getConfigId(), (t) -> new ArrayList<>()).add(result));
        Logger.log("@YELLOWSeparated into " + byExperiment.size() + " experiments!");

        Logger.log("@YELLOWStarting processing, depending on the dataset this might take a minute!");
        int issues = 0;
        for (Map.Entry<Integer, List<ScrapeResult.StoredScrapeResult>> entry : byExperiment.entrySet()) {
            List<ScrapeResult.StoredScrapeResult> results = entry.getValue();
            results.sort(Comparator.comparingLong(ScrapeResult.StoredScrapeResult::getTime));
            int configIssues = 0;
            Logger.log("@YELLOWStarting config ID " + entry.getKey());

            for (int i = 0; i < results.size(); i++) {
                ScrapeResult.StoredScrapeResult result = results.get(i);
                if (!result.isTimedOut()) {
                    continue;
                }

                int surroundingTimeouts = 0;
                int b = i - 1;
                while (b >= 0 && isNetworkIssueTimeout(results.get(b))) {
                    if (!results.get(b).getWebPage().getDomain().equalsIgnoreCase(result.getWebPage().getDomain())) {
                        surroundingTimeouts += 1;
                    }
                    b--;
                }

                int f = i + 1;
                while (f < results.size() && isNetworkIssueTimeout(results.get(f))) {
                    if (!results.get(f).getWebPage().getDomain().equalsIgnoreCase(result.getWebPage().getDomain())) {
                        surroundingTimeouts += 1;
                    }
                    f++;
                }

                if (surroundingTimeouts >= 3) {
                    Logger.log("@REDFound a network issue around " + result.getId() + " surrounding: " + surroundingTimeouts);
                    issues += 1;
                    configIssues += 1;
                    result.setNetworkIssues();
                }
            }

            Logger.log("@GREENFinished config ID " + entry.getKey());
            Logger.log("@GREENResults: " + configIssues + "/" + results.size() + " had network issues");
        }

        Logger.log("@GREENFinished analyzing " + allResults.size() + " scrapes for network issues, found " + issues);
        fixTimestamps();
    }

    public static boolean isNetworkIssueTimeout(ScrapeResult result) {
        if (result.isError()) {
            return true;
        }

        if (!result.isTimedOut()) {
            return false;
        }

        if (result.isNetworkIssues()) {
            return true;
        }

        return false;
    }

    public boolean fixTimestamps() throws SQLException {
        Logger.log("@GREENStarting timestamp fix scan ");
        List<ScrapeResult.StoredScrapeResult> allResults = getAllResults(ScrapeResult.StoredScrapeResult::getId);

        boolean changed = false;
        for (int i = 1; i < allResults.size() - 1; i++) {
            ScrapeResult.StoredScrapeResult result = allResults.get(i);
            ScrapeResult.StoredScrapeResult prev = allResults.get(i - 1);
            ScrapeResult.StoredScrapeResult next = allResults.get(i + 1);
            if (prev != null && prev.getWebPage().getId() == result.getWebPage().getId()
                    && result.getTime() != prev.getTime()
                    && (result.getTime() > next.getTime() || prev.getTime() > next.getTime())) {
                Logger.log("@REDFound fucked timestamp on " + result.getId() + " + " + prev.getId());

                long time = Math.min(prev.getTime(), result.getTime());
                prev.setTime(time);
                result.setTime(time);
                DatabaseManager.getInstance().updateTimestamp(result, prev);
                changed = true;
            }
        }

        if (changed) {
            Logger.log("@REDRepeating time scan due to changes! ");
            fixTimestamps();
        }

        Logger.log("@GREENFixed all timestamps (hopefully)");
        return false;
    }

    private List<ScrapeResult.StoredScrapeResult> getAllResults(ToLongFunction<ScrapeResult.StoredScrapeResult> comp) throws SQLException {
        Logger.log("@YELLOWLets do a mega database request!");
        List<ScrapeResult.StoredScrapeResult> allResults = DatabaseManager.getInstance().getAllScrapeResultsButWithoutContent();
        Logger.log("@YELLOWFound " + allResults.size() + " scrapes in total!");
        allResults.sort(Comparator.comparingLong(comp));
        Logger.log("@YELLOWSorted " + allResults.size() + " results");
        return allResults;
    }

    public void summarizeAll() {
        List<StoredComparison> comparisons;
        try {
            comparisons = DatabaseManager.getInstance().getAllComparisons();
        } catch (SQLException e) {
            Logger.log("@REDError grabbing all comparisons from DB");
            Logger.log(e);
            return;
        }

        Logger.log("@YELLOWGrabbed " + comparisons.size() + " comparisons from storage! Wow that is a lot of data!");
        Logger.log("@YELLOWStarting by reducing to base-domains");
        int domains = reduceDomains(comparisons.stream().map(StoredComparison::getPage).collect(Collectors.toList()));
        Logger.log("@YELLOWFound " + domains + " total domains!");
        Map<Integer, List<StoredComparison>> byExperiment = new HashMap<>();
        for (StoredComparison comp : comparisons) {
            List<StoredComparison> list = byExperiment.computeIfAbsent(comp.getExperiment(), (e) -> new ArrayList<>());
            list.add(comp);
        }

        Logger.log("@YELLOWDivided into " + byExperiment.size() + " experiments");
        for (Map.Entry<Integer, List<StoredComparison>> entry : byExperiment.entrySet()) {
            int experiment = entry.getKey();
            List<StoredComparison> list = entry.getValue();
            Logger.log("@RED-------------------------");
            Logger.log("@YELLOWSummarizing experiment " + experiment + " with " + list.size() + " comparisons!");
            summarizeExperiment(experiment, list);
            Logger.log("@RED-------------------------");
        }

        Logger.log("@RED-------------------------");
        Logger.log("@YELLOWOverall results: (excl experiment 5 & 6)");
        summarizeExperiment(-1, comparisons.stream().filter(comp -> comp.getExperiment() != 5 && comp.getExperiment() != 6).collect(Collectors.toList()));
        Logger.log("@RED-------------------------");
    }

    private void summarizeExperiment(int experiment, List<StoredComparison> comparisons) {
        Map<String, DomainSummary> byDomain = new HashMap<>();
        Map<BlockType, Integer> blockCount = new HashMap<>();
        for (BlockType type : BlockType.values()) {
            blockCount.put(type, 0);
        }

        for (StoredComparison comp : comparisons) {
            DomainSummary summary = byDomain.computeIfAbsent(comp.getPage().getDomain(), DomainSummary::new);
            summary.add(comp);

            BlockType verdict = comp.getVerdict();
            blockCount.put(verdict, blockCount.get(verdict) + 1);
        }

        Logger.log("@YELLOWExperiment " + experiment + " has " + byDomain.size() + " distinct domains");
        byDomain.values().forEach(DomainSummary::summarize);
        for (DomainVerdict verdict : DomainVerdict.values()) {
            Logger.log("@YELLOWVerdict " + verdict + ": " + byDomain.values().stream()
                    .filter(summary -> summary.getVerdict() == verdict).count());
        }

        Logger.log("@YELLOWAll block types: " + blockCount.entrySet().stream()
                .map(entry -> entry.getKey() + ": " + entry.getValue())
                .collect(Collectors.joining(" | ")));

        File output = new File("output/summary-" + experiment + ".csv");
        if (output.getParentFile().mkdirs()) {
            Logger.log("Created dir for summary csv");
        }

        try (PrintWriter writer = new PrintWriter(output)) {
            byDomain.values().forEach(summary -> {
                writer.println(summary.getDomain() + "," + summary.getVerdict() + "," + summary.getBlockPercentage()
                        + "," + summary.getComparisonCount() + "," + summary.getBlocks());
            });

            writer.flush();
            Logger.log("@YELLOWWrote summary to " + output.getName());
        } catch (FileNotFoundException e) {
            Logger.log("@REDError writing summary csv");
            Logger.log(e);
        }
    }

    private int reduceDomains(List<WebPage> pages) {
        // Shortest domains first, those will be our "base" domains for the rest
        pages.sort(Comparator.comparingInt(page -> page.getDomain().length()));
        List<String> domains = new ArrayList<>();

        for (WebPage page : pages) {
            boolean foundDomain = false;
            for (String dom : domains) {
                if (page.getDomain().equals(dom) || page.getDomain().endsWith("." + dom)) {
                    foundDomain = true;
                    page.setDomain(dom);
                }
            }

            if (!foundDomain) {
                domains.add(page.getDomain());
            }
        }

        return domains.size();
    }

    public static AnalysisManager getInstance() {
        if (instance == null) {
            instance = new AnalysisManager();
        }

        return instance;
    }

}
