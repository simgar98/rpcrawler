/*
 *     RPCrawler
 *     Copyright (C) 2021  Jurgen Mulder
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.simgar98.crawler.manager;

import com.simgar98.crawler.Constants;
import com.simgar98.crawler.scheduler.CrawlerAsyncScheduler;
import com.simgar98.crawler.struct.*;
import com.simgar98.crawler.util.LinkUtils;
import com.simgar98.crawler.util.Logger;
import lombok.Getter;

import java.io.File;
import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

public class DatabaseManager {

    @Getter private Connection con;
    @Getter private boolean connected;

    private static DatabaseManager instance;

    private DatabaseManager() {
        instance = this;
        log("Starting database manager");

        connected = attemptConnect();

        CrawlerAsyncScheduler.submitRepeating(this::ensureConnection, 15000L, false);
    }

    public static DatabaseManager getInstance() {
        if (instance == null) {
            new DatabaseManager();
        }

        return instance;
    }

    private boolean attemptConnect() {
        log("Attempting to connect to database");
        try {
            Properties cProps = new Properties();
            cProps.put("user", Constants.getMYSQL_USER());
            cProps.put("password", Constants.getMYSQL_PASSWORD());
            cProps.put("autoReconnect", "true");
            cProps.put("maxReconnects", "5");
            con = DriverManager.getConnection("jdbc:mysql://" + Constants.getMYSQL_HOST() + ":" +
                    Constants.getMYSQL_PORT() + "/" + Constants.getMYSQL_DATABASE() +
                    "?autoReconnect=true", cProps);
            log("Connected");
        } catch (Exception e) {
            log("Error establishing MySQL connection");
            log(e);
            return false;
        }

        return true;
    }

    private void ensureConnection() {
        try {
            if (con.isClosed() || !con.isValid(1000)) {
                Logger.log("@REDWe have to reconnect to the db!");
                attemptConnect();
            }
        } catch (SQLException e) {
            Logger.log("Error ensuring connection");
            Logger.log(e);
        }
    }

    public void storeScrapeResults(List<ScrapeResult> scrapes) {
        ensureConnection();
        Long time = System.currentTimeMillis();
        scrapes.forEach(scrape -> storeScrapeResult(scrape, time));
    }

    private void storeScrapeResult(ScrapeResult scrape, Long time) {
        try (PreparedStatement statement = con.prepareStatement("INSERT INTO scrapes (webpage_id, timestamp, config, title, content, visible_text, screenshot, time_taken) values (?, ?, ?, ?, ?, ?, ?, ?)",
                Statement.RETURN_GENERATED_KEYS)) {
            int pageId = getWebpageId(scrape.getUrl());
            statement.setInt(1, pageId);
            statement.setTimestamp(2, new Timestamp(time));
            statement.setInt(3, scrape.getConfigId());
            statement.setString(4, scrape.getTitle().length() >= 254 ? scrape.getTitle().substring(0, 254) : scrape.getTitle());
            statement.setString(5, scrape.getContent());
            statement.setString(6, scrape.getVisibleText());
            statement.setString(7, scrape.getScreenshot() != null ? scrape.getScreenshot().getName() : "NULL");
            statement.setInt(8, scrape.isTimedOut() ? -1 : (int) scrape.getTimeTaken());
            statement.execute();

            try (ResultSet result = statement.getGeneratedKeys()) {
                if (!result.next()) {
                    log("IDK how but no key for inserted scrape");
                    return;
                }

                scrape.setId(result.getInt(1));
                log("Stored scrape of " + scrape.getUrl() + " as ID " + scrape.getId() + " on page " + pageId);
            }
        } catch (SQLException e) {
            log("@REDError inserting scrape result");
            log(e);
        }
    }

    private int getWebpageId(String url) throws SQLException {
        if (url.length() >= 255) {
            url = url.substring(0, 254);
        }

        try (PreparedStatement statement = con.prepareStatement("SELECT id FROM webpages WHERE url LIKE ?")) {
            statement.setString(1, url);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return resultSet.getInt("id");
                }

                return insertWebpage(url);
            }
        }
    }

    private int insertWebpage(String url) throws SQLException {
        log("Inserting webpage ID for URL " + url);
        String domain = LinkUtils.getDomain(url);

        try (PreparedStatement statement = con.prepareStatement("INSERT INTO webpages (url, domain) VALUES (?, ?)",
                Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, url);
            statement.setString(2, domain);
            statement.execute();

            try (ResultSet result = statement.getGeneratedKeys()) {
                if (!result.next()) {
                    log("@REDNo id for webpage " + url + " generated");
                    return -1;
                }

                return result.getInt(1);
            }
        }
    }

    public void updateTimeTaken(ScrapeResult.StoredScrapeResult result) throws SQLException  {
        log("Updating time taken of " + result.getId());
        try (PreparedStatement statement = con.prepareStatement("UPDATE scrapes SET time_taken=?, timestamp=? WHERE scrape_id=?")) {
            statement.setInt(1, (int) result.getTimeTaken());
            statement.setTimestamp(2, new Timestamp(result.getTime()));
            statement.setInt(3, result.getId());
            statement.execute();
        }
    }

    public void updateTimestamp(ScrapeResult.StoredScrapeResult... results) throws SQLException {
        try (PreparedStatement statement = con.prepareStatement("UPDATE scrapes SET timestamp=? WHERE scrape_id=?")) {
            for (ScrapeResult.StoredScrapeResult result : results) {
                statement.setTimestamp(1, new Timestamp(result.getTime()));
                statement.setInt(2, result.getId());
                statement.addBatch();
            }

            statement.executeBatch();
        }
    }

    public List<WebPage> getPages(String domain) throws SQLException {
        ensureConnection();
        try (PreparedStatement statement = con.prepareStatement("SELECT * FROM webpages WHERE domain LIKE ?")) {
            statement.setString(1, "%" + domain + "%");
            try (ResultSet result = statement.executeQuery()) {
                List<WebPage> pages = new ArrayList<>();
                while (result.next()) {
                    pages.add(
                            new WebPage(result.getInt("id"),
                                    result.getString("url"),
                                    result.getString("domain")));
                }

                return pages;
            }
        }
    }

    public List<ScrapeResult.StoredScrapeResult> getResults(WebPage webPage) throws SQLException {
        ensureConnection();
        try (PreparedStatement statement = con.prepareStatement("SELECT * FROM scrapes WHERE webpage_id=?")) {
            statement.setInt(1, webPage.getId());
            try (ResultSet result = statement.executeQuery()) {
                List<ScrapeResult.StoredScrapeResult> scrapes = new ArrayList<>();
                while (result.next()) {
                    int id = result.getInt("scrape_id");
                    long time = result.getTimestamp("timestamp").getTime();
                    int config = result.getInt("config");
                    String title = result.getString("title");
                    String content = result.getString("content");
                    String visibleText = result.getString("visible_text");
                    String fileName = result.getString("screenshot");
                    int timeTaken = result.getInt("time_taken");
                    scrapes.add(new ScrapeResult.StoredScrapeResult(id, webPage, title, timeTaken, time, config, content, visibleText,
                            new File("logs" + File.separatorChar + "screenshot" + File.separatorChar + fileName)));
                }

                return scrapes;
            }
        }
    }

    public List<ScrapeResult.StoredScrapeResult> getAllScrapeResultsButWithoutContent() throws SQLException {
        ensureConnection();
        try (PreparedStatement statement = con.prepareStatement("SELECT scrape_id, webpage_id, timestamp, config, title, screenshot, time_taken FROM scrapes")) {
            try (ResultSet result = statement.executeQuery()) {
                List<ScrapeResult.StoredScrapeResult> scrapes = new ArrayList<>();
                while (result.next()) {
                    int id = result.getInt("scrape_id");
                    int webpageId = result.getInt("webpage_id");
                    long time = result.getTimestamp("timestamp").getTime();
                    int config = result.getInt("config");
                    String title = result.getString("title");
                    String content = "HIDDEN";
                    String visibleText = "HIDDEN";
                    String fileName = result.getString("screenshot");
                    int timeTaken = result.getInt("time_taken");

                    WebPage webPage = getWebPage(webpageId);
                    if (webPage == null) {
                        Logger.log("@REDWe somehow found a scrape result without a defined webpage (that shouldnt be possible with db constraints/FKs)");
                        Logger.log("@REDInfo: " + id + "/" + webpageId + "//" + time);
                        continue;
                    }

                    scrapes.add(new ScrapeResult.StoredScrapeResult(id, webPage, title, timeTaken, time, config, content, visibleText,
                            new File("logs" + File.separatorChar + "screenshot" + File.separatorChar + fileName)));
                }

                return scrapes;
            }
        }
    }

    public void storeComparisons(List<ScrapeComparison> comparisons) throws SQLException {
        ensureConnection();
        try (PreparedStatement statement = con.prepareStatement("REPLACE INTO results " +
                "(scrape_id_a, scrape_id_b, verdict, block_types, compared_images) VALUES (?, ?, ?, ?, ?)")) {
            for (ScrapeComparison comp : comparisons) {
                statement.setInt(1, comp.getA().getId());
                statement.setInt(2, comp.getB().getId());
                statement.setString(3, comp.getFinalVerdict().toString());
                statement.setString(4, comp.getBlocks().stream().map(BlockType::toString).collect(Collectors.joining(",")));
                statement.setString(5, comp.getScreenshotCompared() != null ? comp.getScreenshotCompared().getName() : null);
                statement.addBatch();
            }

            statement.executeBatch();
        }
    }

    public void storeManualVerification(ScrapeComparison comp, BlockType verdict) throws SQLException {
        ensureConnection();
        try (PreparedStatement statement = con.prepareStatement("REPLACE INTO manual " +
                "(scrape_id_a, scrape_id_b, verdict) VALUES (?, ?, ?)")) {

            statement.setInt(1, comp.getA().getId());
            statement.setInt(2, comp.getB().getId());
            statement.setString(3, verdict.toString());
            statement.execute();
        }
    }

    public WebPage getWebPageOfScrape(int scrapeId) throws SQLException {
        ensureConnection();
        try (PreparedStatement statement = con.prepareStatement("SELECT * FROM scrapes WHERE scrape_id=?")) {
            statement.setInt(1, scrapeId);
            try (ResultSet result = statement.executeQuery()) {
                List<ScrapeResult.StoredScrapeResult> scrapes = new ArrayList<>();
                while (result.next()) {
                    return getWebPage(result.getInt("webpage_id"));
                }

                return null;
            }
        }
    }

    private WebPage getWebPage(int pageId) throws SQLException {
        try (PreparedStatement statement = con.prepareStatement("SELECT * FROM webpages WHERE id = ?")) {
            statement.setInt(1, pageId);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return new WebPage(resultSet.getInt("id"),
                            resultSet.getString("url"),
                            resultSet.getString("domain"));
                }

                return null;
            }
        }
    }

    public List<Integer> getResultsWithVerdict(BlockType verdict, long maxAge) throws SQLException {
        try (PreparedStatement statement = con.prepareStatement("SELECT scrape_id_a, manual FROM results_verified WHERE verdict LIKE ? AND timestamp >= ? AND (manual is NULL)")) {
            statement.setString(1, verdict.toString());
            statement.setTimestamp(2, new Timestamp(System.currentTimeMillis() - maxAge));
            try (ResultSet resultSet = statement.executeQuery()) {
                List<Integer> list = new ArrayList<>();
                while (resultSet.next()) {
                    list.add(resultSet.getInt("scrape_id_a"));
                }

                return list;
            }
        }
    }

    public List<StoredComparison> getAllComparisons() throws SQLException {
        // Now that's a fucking query and a half right there.
        // Could I have made a view containing this info? Yes.
        // Should I have done that instead? Yes, probably.
        // Did I feel like it? No.
        try (PreparedStatement statement = con.prepareStatement("SELECT *, (SELECT config FROM scrapes as d WHERE d.scrape_id=a.scrape_id_a) as config_a, (SELECT config FROM scrapes as d WHERE d.scrape_id=a.scrape_id_b) as config_b, (SELECT verdict FROM manual as e WHERE e.scrape_id_a = a.scrape_id_a and e.scrape_id_b = e.scrape_id_b) as verdict_manual FROM (results AS a JOIN webpages AS b ON b.id = (SELECT webpage_id FROM scrapes AS c WHERE c.scrape_id = a.scrape_id_a))")) {
            try (ResultSet result = statement.executeQuery()){
                List<StoredComparison> comparisons = new ArrayList<>();
                while (result.next()) {
                    WebPage webPage = new WebPage(result.getInt("id"), result.getString("url"),
                            result.getString("domain"));
                    BlockType verdict = BlockType.valueOf(result.getString("verdict"));
                    if (result.getString("verdict_manual") != null) {
                        verdict = BlockType.valueOf(result.getString("verdict_manual"));
                    }

                    comparisons.add(new StoredComparison(result.getInt("scrape_id_a"), result.getInt("scrape_id_b"),
                            Math.max(result.getInt("config_a"), result.getInt("config_b")),
                            verdict, webPage, result.getString("block_types")));
                }

                return comparisons;
            }
        }
    }

    private void log(String msg) {
        Logger.log(msg);
    }

    private void log(Exception e) {
        Logger.log(e);
    }

}

