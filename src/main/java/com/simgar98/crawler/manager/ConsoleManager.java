/*
 *     RPCrawler
 *     Copyright (C) 2021  Jurgen Mulder
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.simgar98.crawler.manager;

import com.simgar98.crawler.Constants;
import com.simgar98.crawler.scheduler.CrawlerAsyncScheduler;
import com.simgar98.crawler.scrape.ScraperProfile;
import com.simgar98.crawler.struct.BlockType;
import com.simgar98.crawler.struct.ScrapeComparison;
import com.simgar98.crawler.swing.InspectionGUI;
import com.simgar98.crawler.util.LinkUtils;
import com.simgar98.crawler.util.Logger;

import java.awt.*;
import java.awt.event.WindowEvent;
import java.io.*;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.stream.Collectors;

public enum ConsoleManager {

    INSTANCE;

    private Queue<Integer> resultQueue;
    private InspectionGUI inspectionGUI;

    ConsoleManager() {
        startListening();
        resultQueue = new ArrayBlockingQueue<>(1);
        Logger.log("Initialized console");

        Logger.log("@WHITE    RPCrawler  Copyright (C) 2021  Jurgen Mulder");
        Logger.log("@WHITE    This program comes with ABSOLUTELY NO WARRANTY; for details go to https://www.gnu.org/licenses/gpl-3.0.html");
        Logger.log("@WHITE    This is free software, and you are welcome to redistribute it under certain conditions.");
    }

    private void startListening() {
        ConsoleListeningThread listenThread = new ConsoleListeningThread();
        listenThread.start();
    }

    public class ConsoleListeningThread extends Thread {
        public void run() {
            InputStreamReader consoleIn = new InputStreamReader(System.in);

            String read;
            BufferedReader buff = new BufferedReader(consoleIn);

            try {
                read = buff.readLine();

                do {
                    try {
                        handle(read);
                    } catch (Exception e) {
                        sendMessage("@REDCouldnt handle command " + read);
                        Logger.log(e);
                    }

                    read = buff.readLine();
                } while (!read.equals("STOP"));
            } catch (IOException e) {
                Logger.log(e);
            }
        }
    }

    public void handle(String msg) {
        String[] split = msg.split(" ");
        String cmd = split[0];

        sendMessage("@YELLOWCommand executed: " + msg);
        switch (cmd.toUpperCase()) {
            case "HELP":
            case "COMMANDS":
            case "COMMAND":
                sendHelp();
                return;
            case "TESTSCRAPER":
            case "SCRAPERTEST":
            case "TESTSCRAPE":
                runScraperTest();
                return;
            case "SCRAPE":
            case "SCRAPER":
            case "SCRAPELIST":
            case "RUNLIST":
            case "SCRAPERLIST":
            case "LISTSCRAPE":
            case "SCRAPEFILE":
            case "FILESCRAPE":
                runScrape(split);
                return;
            case "ANALYZE":
            case "ANALYSIS":
            case "ANAL":
                runAnalysis();
                return;
            case "SHOW":
            case "S":
                showComparison(split);
                return;
            case "SETEXPERIMENT":
            case "EXPERIMENT":
            case "INDEX":
                setExperimentId(split);
                return;
            case "SETPROFILE":
            case "SETSCRAPER":
            case "PROFILE":
            case "SCRAPERPROFILE":
            case "SCRAPEPROFILE":
            case "PROF":
            case "P":
                setScraperProfile(split);
                return;
            case "SCAN":
            case "SCANNETWORK":
            case "NETWORKSCAN":
            case "FIXNETWORK":
            case "NETWORK":
                analyzeNetwork();
                return;
            case "TESTLINKS":
            case "TESTLINK":
                listLinkScrapes(split);
                return;
            case "CRAWLLINKS":
            case "CRAWL":
            case "SCRAPELINKS":
            case "SCRAPELINK":
            case "LINKSCRAPE":
            case "LINKSSCRAPE":
            case "FINDLINKS":
            case "FINDLINK":
                runLinkCrawl(split);
                return;
            case "TESTANALYZE":
            case "TESTANALYSIS":
            case "TESTA":
                testAnalysis(split);
                return;
            case "FIXTIMESTAMPS":
            case "TIME":
            case "STAMPS":
            case "TIMESTAMPS":
            case "TIMESTAMP":
            case "FIXTIME":
                fixTimestamps();
                return;
            case "SUMMARIZE":
            case "SUMMARY":
            case "SUM":
                summarize();
                return;

            case "Q":
            case "QUEUE":
            case "LOADQ":
            case "LOADQUEUE":
                loadQueue(split, 1.0);
                return;
            case "RQ":
            case "RANDOMQ":
            case "RANDOMQUEUE":
            case "RQUEUE":
                loadQueue(split, 0.5);
                return;
            case "N":
            case "NEXT":
                nextInQueue();
                return;
        }

        sendHelp();
    }

    private void nextInQueue() {
        if (resultQueue.isEmpty()) {
            sendMessage("@GREENThere is nothing in the queue! Either you just finished :) \n" +
                    "or forgot to start :(");
            return;
        }

        Integer id = resultQueue.remove();
        sendMessage("@YELLOWPopped " + id + " to compare... " + resultQueue.size() + " to go after this!");
        showComparison(new String[]{"S", id + ""});
        if (resultQueue.isEmpty()) {
            sendMessage("         ");
            sendMessage("@GREENThat is the last one! Wooooo!");
            sendMessage("         ");
        }
    }

    private void loadQueue(String[] split, double fraction) {
        if (split.length < 2) {
            sendMessage("@REDNot enough arguments!");
            return;
        }

        BlockType verdict;
        try {
            verdict = BlockType.valueOf(split[1].toUpperCase());
        } catch (NoSuchElementException | IllegalArgumentException e) {
            sendMessage("@REDInvalid BlockType! Options: " + Arrays.stream(BlockType.values())
                    .map(BlockType::toString).collect(Collectors.joining(", ")));
            return;
        }

        long maxAge = Integer.MAX_VALUE;
        if (split.length >= 3) {
            try {
                maxAge = Long.parseLong(split[2]) * 60 * 60 * 1000;
                SimpleDateFormat format = new SimpleDateFormat("dd/MMM/YY - HH:MM:SS");
                sendMessage("@GREENLooking for results since " + format.format(System.currentTimeMillis() - maxAge));
            } catch (NumberFormatException e) {
                sendMessage("@REDInvalid max age! Use a number (hours)!");
                return;
            }
        }

        List<Integer> results;
        try {
            results = DatabaseManager.getInstance().getResultsWithVerdict(verdict, maxAge);
        } catch (SQLException e) {
            sendMessage("@REDError querying DB");
            Logger.log(e);
            return;
        }

        int toKeep = (int) Math.ceil(results.size() * fraction);
        Collections.shuffle(results);
        resultQueue = new ArrayBlockingQueue<>(Math.max(toKeep, 1));
        results.stream()
                .limit(toKeep)
                .forEach(i -> resultQueue.add(i));
        sendMessage("@GREENSelected " + resultQueue.size() + " results from " + results.size() + " total!");
        sendMessage("@YELLOWUse 'N' to load the first one!");

        if (inspectionGUI != null) {
            sendMessage("Closing old inspection GUI");
            inspectionGUI.dispatchEvent(new WindowEvent(inspectionGUI, WindowEvent.WINDOW_CLOSING));
        }

        sendMessage("Opening inspection GUI");
        inspectionGUI = new InspectionGUI(this);
    }

    private void fixTimestamps() {
        try {
            AnalysisManager.getInstance().fixTimestamps();
        } catch (SQLException e) {
            sendMessage("@REDIssue analyzing network issues");
            Logger.log(e);
        }
    }

    private void analyzeNetwork() {
        try {
            AnalysisManager.getInstance().analyzeNetworkIssues();
        } catch (SQLException e) {
            sendMessage("@REDIssue analyzing network issues");
            Logger.log(e);
        }
    }

    private void setExperimentId(String[] split) {
        if (split.length < 2) {
            sendMessage("@REDNot enough arguments!");
            return;
        }

        int id;
        try {
            id = Integer.parseInt(split[1]);
        } catch (NumberFormatException e) {
            sendMessage("@REDInvalid number!");
            return;
        }

        Constants.setEXPERIMENT_JAP_ID(id);
        sendMessage("@GREENSet JAP experiment ID to " + Constants.getEXPERIMENT_JAP_ID());
    }

    private void setScraperProfile(String[] split) {
        if (split.length < 2) {
            sendMessage("@REDNot enough arguments!");
            return;
        }

        ScraperProfile profile;
        try {
            profile = ScraperProfile.valueOf(split[1].toUpperCase());
        } catch (IllegalArgumentException e) {
            sendMessage("@REDInvalid ScrapeProfile! Options: " + Arrays.stream(ScraperProfile.values())
                    .map(ScraperProfile::toString).collect(Collectors.joining(", ")));
            return;
        }

        Constants.setEXPERIMENT_PROFILE(profile);
        sendMessage("@GREENSet scrape profile for experiments to " + profile);
    }

    private void showComparison(String[] split) {
        if (split.length < 2) {
            sendMessage("@REDNot enough arguments!");
            return;
        }

        int a;
        try {
            a = Integer.parseInt(split[1]);
        } catch (NumberFormatException e) {
            sendMessage("@REDInvalid ID!");
            return;
        }

        ScrapeComparison comparison = AnalysisManager.getInstance().getComparison(a);
        sendMessage("============================");
        sendMessage("@GREENComparison of " + comparison.getName() + " - Exp: " + comparison.getA().getConfigId() +
                " vs " + comparison.getB().getConfigId());
        sendMessage("Webpage: @CYAN" + comparison.getWebPage().getId() + " // " + comparison.getWebPage().getUrl());
        sendMessage("Verdict: @CYAN" + comparison.getFinalVerdict());
        sendMessage("Blocks: @CYAN" + comparison.getBlocks().stream()
                .map(BlockType::toString)
                .collect(Collectors.joining(", ")));
        sendMessage("---");
        sendMessage("DOM Size: @CYAN" + comparison.getDomSizeSimilarity());
        sendMessage("DSS: @CYAN" + comparison.getSimpleDomSimilarity());
        sendMessage("TLS: @CYAN" + comparison.getTextLengthSimilarity());
        sendMessage("Captcha Diff: @CYAN" + comparison.isCaptchaDifference() + " - " +
                comparison.getA().hasCaptcha() + " vs " + comparison.getB().hasCaptcha());
        sendMessage("Error Diff: @CYAN" + comparison.isErrorDifference() + " - " +
                comparison.getA().isError() + " vs " + comparison.getB().isError());
        if (comparison.isErrorDifference()) {
            sendMessage("Errors/Titles: @CYAN" + comparison.getA().getTitle() + " vs " + comparison.getB().getTitle());
        }
        sendMessage("Screen Diff: @CYAN" + comparison.getScreenshotDifference() +
                (comparison.getScreenshotDifference() < 0 ? " @PURPLEThis comparison was skipped as the DOM Size & DSS are too similar" : ""));
        sendMessage("Timeout Diff: @CYAN" + comparison.isTimeoutDifference() + " - " +
                comparison.getA().isTimedOut() + " vs " + comparison.getB().isTimedOut() + " " +
                comparison.getA().getTimeTaken() + " vs " + comparison.getB().getTimeTaken());
        sendMessage("Is in network issue time: @CYAN" +
                comparison.getA().isNetworkIssues() + " vs " + comparison.getB().isNetworkIssues() +
                " @YELLOWMatches: @CYAN" + AnalysisManager.isNetworkIssueTimeout(comparison.getA()) + " vs " +
                AnalysisManager.isNetworkIssueTimeout(comparison.getB()));
        if (comparison.getBlocks().contains(BlockType.BLOCK_PAGE_ADBLOCK)) {
            sendMessage("Adblocks: @CYAN" + comparison.getA().isAdBlocked() + " vs " + comparison.getB().isAdBlocked());
        }
        sendMessage("============================");

        if (inspectionGUI != null && inspectionGUI.isActive()) {
            Logger.log("Opening in GUI");
            inspectionGUI.load(comparison, resultQueue == null ? 0 : resultQueue.size());
        } else {
            openImage(comparison.getA().getScreenshot());
            openImage(comparison.getB().getScreenshot());
            openImage(comparison.getScreenshotCompared());
        }
    }

    private void openImage(File file) {
        if (file == null || !file.exists()) {
            return;
        }

        try {
            Desktop.getDesktop().open(file);
        } catch (Exception e) {
            Logger.log("@REDCould not open image!");
            Logger.log(e);
        }
    }

    private void runAnalysis() {
        sendMessage("Crack an egg on your CPU!");
        CrawlerAsyncScheduler.submit(() -> AnalysisManager.getInstance().analyzeAll(), 0L);
    }

    private void summarize() {
        sendMessage("Time to get some final data!");
        CrawlerAsyncScheduler.submit(() -> AnalysisManager.getInstance().summarizeAll(), 0L);
    }

    private void listLinkScrapes(String[] split) {
        if (split.length < 3) {
            sendMessage("@REDNot enough arguments!");
            return;
        }

        String url = split[1];
        int amount;
        try {
            amount = Integer.parseInt(split[2]);
        } catch (NumberFormatException e) {
            sendMessage("Invalid amount");
            return;
        }

        sendMessage("@GREENScraping " + amount + " links from " + url);
        List<String> links = ScrapeManager.getInstance().scrapeLinks(Collections.singletonList(url), amount, 1);
        sendMessage("@GREENFound @YELLOW" + links.size() + " @GREENlinks!");
        sendMessage(String.join(", ", links));
        sendMessage("@GREENDomains: " + links.stream().map(LinkUtils::getDomain).collect(Collectors.joining(", ")));
    }

    private void runLinkCrawl(String[] split) {
        if (split.length < 4) {
            sendMessage("@REDNot enough arguments!");
            return;
        }

        File file = new File(split[1]);
        if (!file.exists()) {
            sendMessage("@REDFile not found!");
            return;
        }

        int amountPerDomain;
        int amountOfDomains;
        try {
            amountPerDomain = Integer.parseInt(split[2]);
            amountOfDomains = Integer.parseInt(split[3]);
        } catch (NumberFormatException e) {
            sendMessage("Invalid amount");
            return;
        }

        sendMessage("@YELLOWStarting to read file " + file.getName());
        List<String> urls = LinkUtils.readFileToList(file);
        sendMessage("@YELLOWRead " + urls.size() + " URLs from file, starting link scrape, looking for " +
                amountPerDomain + " per domain, and " + amountOfDomains + " good domains...");
        CrawlerAsyncScheduler.submit(() -> {
            List<String> links = ScrapeManager.getInstance().scrapeLinks(urls, amountPerDomain, amountOfDomains);
            sendMessage("@GREENFound " + links.size() + " links!");
            File saveFile = new File(file.getParent(), "links-" + amountPerDomain + "-" + amountOfDomains + ".txt");
            try (FileOutputStream out = new FileOutputStream(saveFile);
                 PrintWriter writer = new PrintWriter(out)) {
                links.forEach(writer::println);
                writer.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
            sendMessage("@GREENSaved to " + saveFile.getName());
        }, 0L);
    }

    private void runScrape(String[] split) {
        if (split.length < 2) {
            sendMessage("@REDNot enough arguments!");
            return;
        }

        File file = new File(split[1]);
        if (!file.exists()) {
            sendMessage("@REDFile not found!");
            return;
        }

        sendMessage("@YELLOWStarting to read file " + file.getName());
        List<String> urls = LinkUtils.readFileToList(file);
        sendMessage("@YELLOWRead " + urls.size() + " URLs from file, starting scrape...");
        CrawlerAsyncScheduler.submit(() -> ScrapeManager.getInstance().compareScrapeList(urls), 0L);
    }

    private void runScraperTest() {
        sendMessage("@GREENStarting scraper test... (setting experiment profile to FIREFOX_REGULAR)");
        Constants.setEXPERIMENT_PROFILE(ScraperProfile.FIREFOX_REGULAR);
        ScrapeManager.getInstance().testScraperFunctionalityList();
    }

    private void testAnalysis(String[] split) {
        if (split.length < 2) {
            sendMessage("@REDNot enough arguments!");
            return;
        }

        String domain = split[1];
        sendMessage("Attempting to look up comparisons of domain " + domain);
        CrawlerAsyncScheduler.submit(() -> AnalysisManager.getInstance().performAnalysis(domain), 0L);
    }

    private void sendHelp() {
        sendMessage("@REDCMD not recognised, current commands are: ");
        sendMessage(" - SCRAPE <file> (Runs an entire list of sites through the scraper");
        sendMessage(" - CRAWL <file> <amount-per-domain> <amount-of-domains> (Runs list to crawl sites and find links)");
        sendMessage(" - ANALYZE (Analyzes literally everything ever submitted. Will nuke your CPU) ");
        sendMessage(" - SHOW <a> <b> (Looks for a comparison and shows its details)");
        sendMessage(" - SETSCRAPER <profile> (Sets the scraper profile that will be used)");
        sendMessage(" - SETPROFILE <int> (Sets the config ID the JAP experiment will use)");
        sendMessage(" - SUMMARIZE <int> (Summarizes the results, taking domains into account)");

        sendMessage("--- Remedy commands ---");
        sendMessage(" - FIXTIMESTAMPS (Tries to fix timestamps of fucked up updates)");
        sendMessage(" - SCAN (Scans for network issues)");

        sendMessage("--- Verification commands ---");
        sendMessage(" - Q <blocktype> [maxage] (Load all results with this blocktype to manually check)");
        sendMessage(" - RQ <blocktype> [maxage] (Load half of Q)");
        sendMessage(" - N (Go to the next item in the queue)");

        sendMessage("--- Testing commands ---");
        sendMessage(" - TESTLINKS <url> <amount> (Finds links on a single webpage)");
        sendMessage(" - TESTSCRAPER (Runs a small list of websites through the scraper)");
        sendMessage(" - TESTANALYZE <domain> (Tries to analyze/compare data found in the database for this specific domain)");
    }

    private void sendMessage(String msg) {
        Logger.log("@YELLOW" + msg);
    }

}
