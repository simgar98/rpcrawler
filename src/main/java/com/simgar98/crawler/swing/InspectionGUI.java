/*
 *     RPCrawler
 *     Copyright (C) 2021  Jurgen Mulder
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.simgar98.crawler.swing;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import com.simgar98.crawler.manager.ConsoleManager;
import com.simgar98.crawler.manager.DatabaseManager;
import com.simgar98.crawler.scheduler.CrawlerAsyncScheduler;
import com.simgar98.crawler.struct.BlockType;
import com.simgar98.crawler.struct.ScrapeComparison;
import com.simgar98.crawler.util.Logger;

import javax.swing.*;
import java.awt.*;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.Collections;

/**
 * Follow the instructions here to get this to properly compile
 * Always use the build stuff from intellij when changing GUI stuffs
 * https://stackoverflow.com/questions/32135018/lambda-expressions-and-java-1-8-in-ideauidesigner
 */
public class InspectionGUI extends JFrame {

    private JPanel mainPanel;
    private JButton buttonNext;
    private JLabel scrapeIds;
    private JLabel checkingNumber;
    private JPanel imageA;
    private JPanel imageB;
    private JScrollPane scrollA;
    private JScrollPane scrollB;
    private JLabel verdict;
    private JCheckBox checkScroll;
    private JButton markButton;
    private JComboBox markBox;
    private JButton innocentButton;
    private JButton guiltyButton;
    private JButton networkButton;
    private JButton markCorrectButton;

    private ScrapeComparison comparison;
    private ConsoleManager consoleManager;

    public InspectionGUI(ConsoleManager consoleManager) {
        super("Compare these pls - Ghetto result comparing GUI");
        this.consoleManager = consoleManager;

        $$$setupUI$$$();
        setContentPane(mainPanel);
        setSize(1920, 1000);
        setLocation(0, 0);
        setVisible(true);
        setResizable(true);

        checkingNumber.setText("Press next to start");
        scrapeIds.setText("");

        buttonNext.addActionListener(actionEvent -> loadNext());
        markButton.addActionListener(actionEvent -> markComparison());
        markCorrectButton.addActionListener(actionEvent -> markComparison(comparison.getFinalVerdict()));
        innocentButton.addActionListener(actionEvent -> markComparison(BlockType.NOT_BLOCKED));
        guiltyButton.addActionListener(actionEvent -> markComparison(BlockType.BLOCK_PAGE));
        networkButton.addActionListener(actionEvent -> markComparison(BlockType.NETWORK_ISSUES));

        // Links A to B, but not the other way around
        // I wanted to do it both ways, but it just errors due to infinite recursion....
        scrollA.getVerticalScrollBar().addAdjustmentListener((e) -> linkScrolls(e.getAdjustable()));

        CrawlerAsyncScheduler.submitRepeating(this::autoScroll, 200L, false);
    }

    private void markComparison() {
        BlockType type = (BlockType) markBox.getSelectedItem();
        markComparison(type);
    }

    private void markComparison(BlockType type) {
        if (comparison == null) {
            return;
        }

        ScrapeComparison comp = this.comparison;
        Logger.log("Marking " + comp.getName() + " as " + type);
        comp.setFinalVerdict(type);
        verdict.setText("Marking as " + type);
        verdict.updateUI();

        CrawlerAsyncScheduler.submit(() -> {
            try {
                DatabaseManager.getInstance().storeManualVerification(comp, type);
                DatabaseManager.getInstance().storeComparisons(Collections.singletonList(comp));
                if (comp.equals(this.comparison)) {
                    SwingUtilities.invokeLater(() -> verdict.setText("Marked as " + type));
                }

                CrawlerAsyncScheduler.submit(() -> {
                    if (comp.equals(this.comparison)) {
                        loadNext();
                    }
                }, 500L);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }, 0L);
    }

    private void autoScroll() {
        if (!isVisible() || !isActive() || !isEnabled()) {
            return;
        }

        if (!checkScroll.isSelected()) {
            return;
        }

        try {
            SwingUtilities.invokeAndWait(() -> {
                int step = 50;
                JScrollBar bar = scrollA.getVerticalScrollBar();
                if (bar.getValue() + step + bar.getVisibleAmount() >= bar.getMaximum()) {
                    bar.setValue(bar.getMinimum());
                    bar.updateUI();
                    return;
                }

                bar.setValue(bar.getValue() + step);
            });
        } catch (InterruptedException | InvocationTargetException e) {
            Logger.log(e);
        }
    }

    private void linkScrolls(Adjustable adjustable) {
        if (!isVisible() || !isActive() || !isEnabled()) {
            return;
        }

        scrollB.getVerticalScrollBar().setValue(adjustable.getValue());
        scrollB.getVerticalScrollBar().updateUI();
    }

    private void loadNext() {
        checkingNumber.setText("Loading...");
        scrapeIds.setText("Loading...");
        CrawlerAsyncScheduler.submit(() -> consoleManager.handle("N"), 0L);
    }

    public void load(ScrapeComparison comparison, int left) {
        this.comparison = comparison;
        ((ImagePanel) imageA).loadImage(comparison.getA().getScreenshot());
        ((ImagePanel) imageB).loadImage(comparison.getB().getScreenshot());

        checkingNumber.setText("Comparisons left: " + left);
        scrapeIds.setText("Comparing: " + comparison.getName());
        verdict.setText("Verdict: " + comparison.getFinalVerdict().toString());
        scrollA.getVerticalScrollBar().setValue(0);
        markBox.setSelectedIndex(0);
    }

    private void createUIComponents() {
        imageA = new ImagePanel();
        imageB = new ImagePanel();

        markBox = new JComboBox(BlockType.values());
        markBox.setSelectedIndex(0);
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        createUIComponents();
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
        mainPanel.setBackground(new Color(-13881546));
        mainPanel.setForeground(new Color(-14737363));
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel1.setAlignmentY(0.5f);
        panel1.setAutoscrolls(false);
        panel1.setBackground(new Color(-13881546));
        panel1.setForeground(new Color(-13881546));
        mainPanel.add(panel1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        scrollA = new JScrollPane();
        scrollA.setHorizontalScrollBarPolicy(31);
        scrollA.setVerticalScrollBarPolicy(22);
        scrollA.setWheelScrollingEnabled(false);
        panel1.add(scrollA, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        imageA.setBackground(new Color(-14803650));
        imageA.setForeground(new Color(-13881546));
        imageA.setMinimumSize(new Dimension(24, 24));
        imageA.setPreferredSize(new Dimension(24, 1080));
        scrollA.setViewportView(imageA);
        scrollB = new JScrollPane();
        scrollB.setHorizontalScrollBarPolicy(31);
        scrollB.setVerticalScrollBarPolicy(22);
        scrollB.setWheelScrollingEnabled(false);
        panel1.add(scrollB, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        imageB.setBackground(new Color(-14803650));
        imageB.setForeground(new Color(-4508364));
        imageB.setMinimumSize(new Dimension(24, 24));
        imageB.setPreferredSize(new Dimension(24, 1080));
        scrollB.setViewportView(imageB);
        final JPanel panel2 = new JPanel();
        panel2.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));
        panel2.setBackground(new Color(-13881546));
        panel2.setEnabled(true);
        panel2.setForeground(new Color(-13486787));
        mainPanel.add(panel2, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_SOUTH, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, 1, new Dimension(-1, 50), new Dimension(-1, 50), new Dimension(-1, 50), 0, false));
        markBox.setBackground(new Color(-9276814));
        markBox.setForeground(new Color(-8487298));
        panel2.add(markBox);
        markButton = new JButton();
        markButton.setBackground(new Color(-9276814));
        markButton.setForeground(new Color(-1));
        markButton.setText("Mark");
        panel2.add(markButton);
        final Spacer spacer1 = new Spacer();
        panel2.add(spacer1);
        checkScroll = new JCheckBox();
        checkScroll.setActionCommand("Scroll");
        checkScroll.setBackground(new Color(-13881546));
        checkScroll.setForeground(new Color(-1));
        checkScroll.setText("Scroll");
        panel2.add(checkScroll);
        verdict = new JLabel();
        verdict.setBackground(new Color(-984321));
        verdict.setForeground(new Color(-1));
        verdict.setHorizontalAlignment(2);
        verdict.setText("Verdict");
        panel2.add(verdict);
        final Spacer spacer2 = new Spacer();
        panel2.add(spacer2);
        checkingNumber = new JLabel();
        checkingNumber.setBackground(new Color(-1250068));
        checkingNumber.setForeground(new Color(-1));
        checkingNumber.setText("Checking x/x");
        panel2.add(checkingNumber);
        final Spacer spacer3 = new Spacer();
        panel2.add(spacer3);
        final Spacer spacer4 = new Spacer();
        panel2.add(spacer4);
        scrapeIds = new JLabel();
        scrapeIds.setBackground(new Color(-1));
        scrapeIds.setForeground(new Color(-1));
        scrapeIds.setText("Scrapes: a vs b");
        panel2.add(scrapeIds);
        final Spacer spacer5 = new Spacer();
        panel2.add(spacer5);
        markCorrectButton = new JButton();
        markCorrectButton.setBackground(new Color(-9276814));
        markCorrectButton.setForeground(new Color(-1));
        markCorrectButton.setText("Cor");
        panel2.add(markCorrectButton);
        networkButton = new JButton();
        networkButton.setBackground(new Color(-9276814));
        networkButton.setForeground(new Color(-4493568));
        networkButton.setText("Network");
        panel2.add(networkButton);
        guiltyButton = new JButton();
        guiltyButton.setBackground(new Color(-9276814));
        guiltyButton.setForeground(new Color(-4521980));
        guiltyButton.setText("Guilty");
        panel2.add(guiltyButton);
        innocentButton = new JButton();
        innocentButton.setBackground(new Color(-9276814));
        innocentButton.setForeground(new Color(-10289310));
        innocentButton.setText("Innocent");
        panel2.add(innocentButton);
        buttonNext = new JButton();
        buttonNext.setBackground(new Color(-9276814));
        buttonNext.setForeground(new Color(-394759));
        buttonNext.setText("Next");
        panel2.add(buttonNext);
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return mainPanel;
    }

}
