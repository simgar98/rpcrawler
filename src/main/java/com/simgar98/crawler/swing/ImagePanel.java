/*
 *     RPCrawler
 *     Copyright (C) 2021  Jurgen Mulder
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.simgar98.crawler.swing;

import com.simgar98.crawler.util.Logger;
import lombok.Getter;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;
import java.io.File;
import java.io.IOException;

public class ImagePanel extends JPanel {

    @Getter private BufferedImage image;

    public ImagePanel() {

    }

    public void loadImage(File file) {
        if (file == null || !file.exists()) {
            image = null;
            return;
        }

        try {
            image = ImageIO.read(file);
        } catch (IOException | IllegalArgumentException e) {
            Logger.log("@REDSomething wrong with an image in a GUI");
            Logger.log(e);
            image = null;
        }

        repaint();
    }

    public void darken(float f) {
        RescaleOp op = new RescaleOp(f, 0, null);
        image = op.filter(image, null);
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        if (image == null) {
            return;
        }

        if (getSize().getHeight() != image.getHeight() || getSize().getWidth() != image.getWidth()) {
            setPreferredSize(new Dimension(image.getWidth(), image.getHeight() + 5));
        }
        graphics.drawImage(image, 0, 0, image.getWidth(), image.getHeight(), Color.PINK, this);
    }

}