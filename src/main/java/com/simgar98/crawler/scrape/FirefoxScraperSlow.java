/*
 *     RPCrawler
 *     Copyright (C) 2021  Jurgen Mulder
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.simgar98.crawler.scrape;

import com.browserup.bup.BrowserUpProxy;
import com.browserup.bup.BrowserUpProxyServer;
import com.browserup.bup.client.ClientUtil;
import com.simgar98.crawler.Constants;
import com.simgar98.crawler.util.Logger;
import org.openqa.selenium.Proxy;

import java.util.concurrent.TimeUnit;

public class FirefoxScraperSlow extends FirefoxScraper {

    private BrowserUpProxy proxy;
    private String proxyAddress;

    public FirefoxScraperSlow() {
        super(5);
    }

    @Override
    protected Proxy getProxy() {
        Logger.log("Enabling BrowserUpProxy");

        if (proxy == null) {
            Logger.log("Building a new BrowserUpProxy");
            proxy = new BrowserUpProxyServer();
            proxy.setReadBandwidthLimit(Constants.getSLOW_PROXY_DOWNSTREAM_LIMIT());
            proxy.setWriteBandwidthLimit(Constants.getSLOW_PROXY_UPSTREAM_LIMIT());
            proxy.setLatency(Constants.getSLOW_PROXY_LATENCY(), TimeUnit.MILLISECONDS);
            proxy.start(0, ClientUtil.getConnectableAddress());
            proxyAddress = ClientUtil.getConnectableAddress().getHostAddress() + ":" + proxy.getPort();
            Logger.log("Set up new BrowserUpProxy on " + proxyAddress);
        }

        return new Proxy().setHttpProxy(proxyAddress)
                .setSslProxy(proxyAddress)
                .setFtpProxy(proxyAddress);
    }

}
