/*
 *     RPCrawler
 *     Copyright (C) 2021  Jurgen Mulder
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.simgar98.crawler.scrape;

import com.simgar98.crawler.Constants;
import com.simgar98.crawler.util.Logger;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;

import java.io.File;

public class FirefoxScraper extends SeleniumScraper<FirefoxDriver> {

    public FirefoxScraper() {
        this(0);
    }

    public FirefoxScraper(int configId) {
        super(configId);
    }

    @Override
    protected FirefoxDriver setupDriver() {
        Logger.log("Setting up new driver");
        FirefoxOptions options = new FirefoxOptions();
        Logger.log("Going to generate proxy");
        Proxy proxy = getProxy();
        Logger.log("Generated proxy to " + proxy);
        options.setProxy(proxy);
        FirefoxProfile profile = new FirefoxProfile();
        if (Constants.isSELENIUM_USE_CACHE()) {
            profile.setPreference("browser.cache.disk.parent_directory", getProfileFolder().getAbsolutePath());
        }
        if (Constants.isSELENIUM_USE_LAZYLOADING()) {
            profile.setPreference("dom.image-lazy-loading.enabled", true);
            profile.setPreference("dom.image-lazy-loading.root-margin.top", 1080);
            profile.setPreference("dom.image-lazy-loading.root-margin.bottom", 0);
            profile.setPreference("dom.image-lazy-loading.root-margin.left", 0);
            profile.setPreference("dom.image-lazy-loading.root-margin.right", 0);
        } else {
            profile.setPreference("dom.image-lazy-loading.enabled", false);
        }
        options.setProfile(profile);
        FirefoxDriver driver = new FirefoxDriver(options);
        driver.installExtension(new File("libs/uBlock-1.35.0@raymondhill.net.xpi").toPath());
        if (Constants.isSELENIUM_USE_LAZYLOADING()) {
            driver.installExtension(new File("libs/lazyloadify-0.0.2.xpi").toPath());
        }
        return driver;
    }

    private File getProfileFolder() {
        File file = new File("caches" + File.separatorChar + "Firefox-" + getConfigId());
        Logger.log("Setting cache to " + file.getName() + " (ID: " + getConfigId() + ")");
        if (file.mkdirs()) {
            Logger.log("Created cache dirs");
        }
        return file;
    }

    @Override
    protected String getDriverFileName() {
        return Constants.getSELENIUM_WEBDRIVER_FIREFOX();
    }

}
