/*
 *     RPCrawler
 *     Copyright (C) 2021  Jurgen Mulder
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.simgar98.crawler.scrape;

import com.simgar98.crawler.Constants;
import com.simgar98.crawler.struct.BlockKeyWord;
import com.simgar98.crawler.struct.ScrapeResult;
import com.simgar98.crawler.util.ChineseUtils;
import com.simgar98.crawler.util.LinkUtils;
import com.simgar98.crawler.util.Logger;
import lombok.Getter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.remote.RemoteWebElement;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;

import static com.simgar98.crawler.util.Logger.log;

public abstract class SeleniumScraper<T extends WebDriver> {

    protected T driver;

    @Getter private int configId;
    @Getter private String currentScrape;
    @Getter private String previousScrape;
    
    public SeleniumScraper(int configId) {
        Logger.log("Starting a seleniumscraper with ID " + configId);
        this.configId = configId;
        System.setProperty("webdriver.gecko.driver", "libs/" + getDriverFileName());

        launchDriver();
    }

    private void launchDriver() {
        if (driver != null) {
            try {
                driver.quit();
            } catch (Exception e) {
                Logger.log("Error trying to quit driver: ");
                Logger.log(e);
            }
        }

        this.driver = setupDriver();
        setStandardDriverProperties();
//        makeDriverSneaky();
    }

    private void setStandardDriverProperties() {
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(Constants.getSELENIUM_LOAD_TIMEOUT_SECONDS(), TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(Constants.getSELENIUM_JS_TIMEOUT_SECONDS(), TimeUnit.SECONDS);
        driver.manage().deleteAllCookies();
    }

    private void makeDriverSneaky() {
        if (!(driver instanceof JavascriptExecutor)) {
            log("Could not set browsers webdriver flag");
            return;
        }

        // This does not seem to properly work, it is worth a shot though
        // Test box on https://aap.ovh/ for testing
        ((JavascriptExecutor) driver).executeScript("Object.defineProperty(navigator, 'webdriver', {get: () => false})");
        ((JavascriptExecutor) driver).executeScript("navigator.webdriver = false");
    }

    protected abstract String getDriverFileName();

    protected abstract T setupDriver();

    public ScrapeResult scrapePageSafe(String url) {
        try {
            url = LinkUtils.fixUrl(url);
            currentScrape = url;
            ScrapeResult result = scrapePage(url);
            currentScrape = null;
            previousScrape = url;
            return result;
        } catch (TimeoutException | JavascriptException e) {
            ScrapeResult result = new ScrapeResult.ScrapeResultTimeout(this, url, driver.getTitle(),
                    driver.getPageSource(), getVisibleTextSafe(), takeScreenshot());
            log("@REDTimed out on URL " + url);
            launchDriver();
            log("@REDRestarted driver");
            return result;
        } catch (WebDriverException e) {
            log("@REDError while trying URL " + url);
            String raw = e.getMessage();
            if (raw.contains("about:neterror?e=netTimeout")) {
                log("@REDLooks like a proper timeout!");
                return new ScrapeResult.ScrapeResultError(this, url, "netTimeout");
            }

            if (raw.contains("about:neterror?e=connectionFailure")) {
                log("@REDLooks like we got blocked!");
                return new ScrapeResult.ScrapeResultError(this, url, "connectionFailure");
            }

            if (raw.contains("about:neterror?e=dnsNotFound")) {
                log("@REDLooks like our DNS does not know about this!");
                return new ScrapeResult.ScrapeResultError(this, url, "dnsNotFound");
            }

            if (raw.contains("about:neterror?e=nssFailure")) {
                log("@REDLooks like an nssFailure!");
                return new ScrapeResult.ScrapeResultError(this, url, "nssFailure");
            }

            if (raw.contains("about:neterror?e=redirectLoop")) {
                log("@REDLooks like we got redirect looped!");
                return new ScrapeResult.ScrapeResultError(this, url, "redirectLoop");
            }

            if (raw.contains("about:neterror?e=proxyConnectFailure")) {
                for (int i = 0; i < 10; i++) {
                    log("@REDI think JAP is down!!!");
                }
                return new ScrapeResult.ScrapeResultError(this, url, "proxyConnectFailure");
            }

            // Some special selenium crash we saw on uol.com.br/adlab
            if (raw.contains("Failed to decode response from marionette")
                    || raw.contains("Tried to run command without establishing a connection")) {
                log("Someone properly crashed our browser! Lets try launching again!");
                launchDriver();
                return new ScrapeResult.ScrapeResultError(this, url, "fullBrowserCrash");
            }

            log(e);
            return new ScrapeResult.ScrapeResultError(this, url, "Unknown Error - " + raw);
        }
    }

    public List<String> scrapeRandomLinks(String url, int amount) {
        ScrapeResult result = scrapePageSafe(url);
        if (result == null
                || result instanceof ScrapeResult.ScrapeResultError
                || result instanceof ScrapeResult.ScrapeResultTimeout) {
            return null;
        }

        int asianCount = ChineseUtils.countIdeoGraphicCharacters(driver.getPageSource());
        int asianTitle = ChineseUtils.countIdeoGraphicCharacters(result.getTitle());
        Logger.log("@YELLOWFound " + asianCount + ":" + asianTitle + " chinese characters on " + url);
        if (!ChineseUtils.isConfirmedNotAsian(url) &&
                (asianCount >= Constants.getMAX_IDEOGRAPHIC_CHARACTERS() || asianTitle >= Constants.getMAX_IDEOGRAPHIC_CHARACTERS_TITLE())) {
            Logger.log("@YELLOWIgnoring this website, too Asian for us!");
            return null;
        }

        List<String> list = new ArrayList<>();
        try {
            String domain = LinkUtils.getDomain(url);
            List<WebElement> links = driver.findElements(By.partialLinkText(""));
            Collections.shuffle(links);
            links.stream()
                    .filter(element -> {
                        // This fired on 4 in over a thousand domains in the crawls.
                        // StaleElementException mainly, but lets catch everything weird.
                        try {
                            return element.isDisplayed();
                        } catch (WebDriverException ignored) {}

                        return false;
                    })
                    .map(webElement -> webElement.getAttribute("href"))
                    .filter(Objects::nonNull)
                    .filter(link -> link.length() > 5)
                    .filter(LinkUtils::isGoodLink)
                    .map(link -> LinkUtils.fixLink(link, domain))
                    .filter(link -> LinkUtils.isSameDomain(link, domain))
                    .limit(amount)
                    .forEach(list::add);
        } catch (Exception e) {
            log("Something went wrong scraping random links on URL " + url);
            log(e);
        }

        return list;
    }

    public ScrapeResult scrapePage(String url) throws WebDriverException {
        securePreviousPageStop();
        safeClearCookiesOtherPage(url);
        long start = System.currentTimeMillis();
        log("Loading url " + url);
        driver.get(url);
        waitForFullLoad();

        long time = (System.currentTimeMillis() - start);
        String title = driver.getTitle();
        log("Title: " + title);
        log("Finished loading " + url + " in " + time + "ms");
        File file = takeScreenshot();

        return new ScrapeResult(this, url, title, time, driver.getPageSource(), getVisibleTextSafe(), file);
    }

    private void securePreviousPageStop() {
        ((JavascriptExecutor) driver).executeScript("window.stop();");
        try {
            Thread.sleep(100L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void waitForFullLoad() {
        try {
            Thread.sleep(2000L);
            if (LinkUtils.isNeedsExtraTime(getCurrentScrape()) || isWaitingPage()) {
                Logger.log("@YELLOWGiving extra time to " + getCurrentScrape());
                Thread.sleep(10000L);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        acceptCookies();
    }

    private boolean isWaitingPage() {
        List<WebElement> challenges = new ArrayList<>();
        challenges.addAll(driver.findElements(By.className("challenge-form")));
        challenges.addAll(driver.findElements(By.id("challenge-form")));
        challenges.addAll(driver.findElements(By.id("cf-content")));

        if (!challenges.isEmpty() && challenges.stream().anyMatch(WebElement::isDisplayed)) {
            Logger.log("@YELLOWFound a challenge form");
            return true;
        }

        String source = driver.getPageSource();
        Document doc = Jsoup.parse(source);
        String text = doc.text().toLowerCase();
        for (String s : BlockKeyWord.CHECKING_PAGE.getSigns()) {
            if (text.contains(s)) {
                Logger.log("@YELLOWFound a checking page keyword, " + s);
                return true;
            }
        }

        return false;
    }

    private String getVisibleTextSafe() {
        try {
            return getVisibleText();
        } catch (WebDriverException ignored) {
            return null;
        }
    }

    /*
     * This *very roughly* selects the text of the page that is visible.
     * It only looks at the first division from the body, after that it does not care
     * but at least its better than parsing it later and including hidden elements..
     */
    private String getVisibleText() throws WebDriverException {
        long start = System.currentTimeMillis();
        WebElement body = driver.findElement(By.tagName("body"));
        int height = driver.manage().window().getSize().getHeight();
        if (body == null) {
            return null;
        }

        StringBuilder builder = new StringBuilder();
        try {
            List<WebElement> list = body.findElements(By.xpath("./*"));
            Logger.log("Getting the list was " + (System.currentTimeMillis() - start) + "ms and it has " + list.size() + " elements");
            for (WebElement element : list) {
                if (element.getLocation().getY() >= height) {
                    continue;
                }

                if (!element.isEnabled() || !element.isDisplayed()) {
                    continue;
                }

                String id = element.getAttribute("id");
                if (id != null && id.length() > 2) {
                    String raw = (String) ((JavascriptExecutor) driver).executeScript(
                            "let SH61A73bZk = document.getElementById(\"" + id + "\"); return SH61A73bZk != null ? SH61A73bZk.innerText : '';");
                    builder.append(raw).append(" [JS] ");
                    continue;
                }
                builder.append(element.getText()).append(" ");
            }
        } catch (Exception ignored) {
//            Logger.log(ignored);
            return null;
        }

        Logger.log("took approx " + (System.currentTimeMillis() - start) + "ms to get text");
        return builder.toString();
    }

    protected File takeScreenshot() {
        String url = driver.getCurrentUrl();
        String screenName = url.replace(".", "-")
                .replace("https://", "")
                .replace("http://", "")
                .replace("/", "")
                .replaceAll("\\?.*", "");

        if (screenName.length() >= 60) {
            screenName = screenName.substring(0, 60);
        }

        int id = 0;
        File target = null;
        while (target == null || target.exists()) {
            target = new File("logs/screenshot/" + screenName + "-" + getConfigId() + "-" + id + ".png");
            id++;
        }

        try {
            // To try to avoid race conditions between threads, shouldnt happen with unique config ids, but just in case
            target.createNewFile();
            TakesScreenshot takes = (TakesScreenshot) driver;
            File file = takes.getScreenshotAs(OutputType.FILE);

            if (target.getParentFile().mkdirs()) {
                Logger.log("Made screenshot folder(s)");
            }

            Files.copy(file.toPath(), target.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            Logger.log("@REDCould not copy screenshot");
            Logger.log(e);
        }

        return target;
    }

    protected Proxy getProxy() {
        return new Proxy().setNoProxy("No Proxy");
    }

    public void clearCookies() {
        driver.manage().deleteAllCookies();
    }

    private void safeClearCookiesOtherPage(String url) {
        if (previousScrape == null) {
            Logger.log("@GREENClearing cookies for fresh start with " + url);
            clearCookies();
            return;
        }

        String prevDomain = LinkUtils.getDomain(previousScrape).toLowerCase();
        String nextDomain = LinkUtils.getDomain(url).toLowerCase();
        String[] splitNext = nextDomain.split("\\.");
        String[] splitPrev = prevDomain.split("\\.");
        if (prevDomain.equals(nextDomain)) {
            Logger.log("Not clearing cookies, going to the same domain!");
            return;
        }

        if (splitNext.length >= 3) {
            String nextDomainNoSub = LinkUtils.removeSubDomain(nextDomain);
            if (prevDomain.equals(nextDomainNoSub)) {
                Logger.log("Not clearing cookies, going to the same domain, but with a subdomain!");
                return;
            }
        }

        // This can falseflag on domains with a "tld" consisting of two parts, eg .com.jp or .com.hk
        // but only if there are two different domains in a row with the same two section tld.. so...
        if (splitPrev.length >= 3) {
            String prevDomainNoSub = LinkUtils.removeSubDomain(prevDomain);
            if (nextDomain.contains(prevDomainNoSub)) {
                Logger.log("Not clearing cookies, going to the same domain, but previous had a subdomain!");
                return;
            }
        }

        Logger.log("@GREENClearing cookies for " + url);
        clearCookies();
    }

    public void quit() {
        driver.quit();
    }

    private void acceptCookies() {
        // accept google cookies, .VDity -> #zV9nZe
        clickCookieButton(By.id("zV9nZe"),
                (e) -> true,
                (p) -> p.getAttribute("class").equalsIgnoreCase("VDity"));

        // Facebook
        clickCookieButton(By.cssSelector("button[id^='" + "u_0_j_" + "']"),
                (e) -> e.getAttribute("data-testid").equalsIgnoreCase("cookie-policy-dialog-accept-button"),
                (p) -> true);

        // whatsapp.com
        clickCookieButton(By.cssSelector("button[id^='" + "u_0_3_" + "']"),
                (e) -> e.getAttribute("data-testid").equalsIgnoreCase("cookie_banner_accept_button"),
                (p) -> true);

        // yahoo.com
        // Not a clean one
        clickCookieButton(By.className("primary"),
                (e) -> e.getAttribute("name").equalsIgnoreCase("agree"),
                (p) -> p.getAttribute("class").equalsIgnoreCase("actions couple"));

        // Instagram.com
        clickCookieButton(By.className("bIiDR"),
                (e) -> e.getAttribute("class").equalsIgnoreCase("aOOlW  bIiDR  "),
                (p) -> p.getAttribute("class").equalsIgnoreCase("_1XyCr"));

        // Reddit.com
        clickCookieButton(By.className("_1tI68pPnLBjR1iHcL7vsee _2iuoyPiKHN3kfOoeIQalDT _10BQ7pjWbeYP63SAPNS8Ts HNozj_dKjQZ59ZsfEegz8 "),
                (e) -> e.getAttribute("role").equalsIgnoreCase("button"),
                (p) -> true);

        // Bing.com
        clickCookieButton(By.id("bnp_btn_accept"),
                (e) -> e.getAttribute("class").equalsIgnoreCase("bnp_btn_accept"),
                (p) -> p.getAttribute("class").equalsIgnoreCase("bnp_action_container"));

        // Youtube.com
        clickCookieButton(By.className("VfPpkd-LgbsSe"),
                (e) -> e.getAttribute("jsname").equalsIgnoreCase("higCR"),
                (p) -> p.getAttribute("class").equalsIgnoreCase("VfPpkd-dgl2Hf-ppHlrf-sM5MNb"));

        // Adobe language
        clickCookieButton(By.className("current-lang"),
                (e) -> e.getAttribute("class").equalsIgnoreCase("locale-modal_button current-lang"),
                (p) -> true);

        // bbc.com
        clickCookieButton(By.className("fc-primary-button"),
                (e) -> e.getAttribute("class").equalsIgnoreCase("fc-button fc-cta-consent fc-primary-button"),
                (p) -> p.getAttribute("class").equalsIgnoreCase("fc-footer-buttons-container"));

        // fandom.com
        // Does not work
        clickCookieButton(By.className("NN0_TB_DIsNmMHgJWgT7U XHcr6qf5Sub2F2zBJ53S_"),
                e -> e.getAttribute("data-tracking-opt-in-accept").equalsIgnoreCase("true"),
                p -> true);
    }

    private WebElement getElement(By by, Predicate<RemoteWebElement> check, Predicate<RemoteWebElement> parentCheck) {
        for (WebElement element : driver.findElements(by)) {
            if (element == null) {
                continue;
            }

            try {
                if (!check.test((RemoteWebElement) element)) {
                    log("Failed element check " + by);
                    continue;
                }

                WebElement parent = element.findElement(By.xpath("./.."));
                if (!parentCheck.test((RemoteWebElement) parent)) {
                    log("Failed parent test " + by);
                    continue;
                }
            } catch (NullPointerException ignored) {
                log("@REDgetElement check NPEd! " + by);
                continue;
            }

            return element;
        }

        return null;
    }

    private void clickCookieButton(By by, Predicate<RemoteWebElement> check, Predicate<RemoteWebElement> parentCheck) {
        try {
            WebElement element = getElement(by, check, parentCheck);
            if (element == null) {
                return;
            }

            element.click();
            log("@GREENAccepted some cookies (" + element + ")");
            try {
                Thread.sleep(1500);
                log("@GREENRe-Loading post-cookie!");
                long reloadStart = System.currentTimeMillis();
                driver.get(getCurrentScrape());
                Thread.sleep(1000);
                log("@GREENRe-Load took " + (System.currentTimeMillis() - reloadStart) + "ms");
            } catch (InterruptedException ignored) {}
        } catch (NoSuchElementException |
                StaleElementReferenceException |
                ElementNotInteractableException ignored) {
            return;
        }
    }

}
