/*
 *     RPCrawler
 *     Copyright (C) 2021  Jurgen Mulder
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.simgar98.crawler.scrape;

import com.simgar98.crawler.Constants;
import com.simgar98.crawler.util.Logger;
import org.openqa.selenium.Proxy;

public class FirefoxScraperJAP extends FirefoxScraper implements AnonymizedScraper {

    public FirefoxScraperJAP() {
        super(Constants.getEXPERIMENT_JAP_ID());
    }

    @Override
    protected Proxy getProxy() {
        Logger.log("Enabling JAP proxy");
        String jap = "127.0.0.1:4001";
        return new Proxy().setHttpProxy(jap)
                .setSslProxy(jap)
                .setFtpProxy(jap);
    }

    @Override
    public int getConfigId() {
        return Constants.getEXPERIMENT_JAP_ID();
    }

}
