/*
 *     RPCrawler
 *     Copyright (C) 2021  Jurgen Mulder
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.simgar98.crawler.util;

import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;

public class DOMUtils {

    public static boolean isCaptchaElement(Element element) {
        // Empty elements don't matter, likely a placeholder to later insert
//        if (element.text().length() <= 1 && element.getAllElements().size() <= 0) {
//            return false;
//        }

        // Check some known classes
        if (element.hasClass("captcha") || element.hasClass("g-recaptcha")
                || element.hasClass("g-recaptcha-response") || element.hasClass("fbx-captcha-container")
                || element.hasClass("reCaptchaInvisibleArea") || element.hasClass("challenge-container")
                || element.hasClass("task-grid")
                || element.classNames().stream().anyMatch(c -> c.toLowerCase().contains("captcha"))) {
            return true;
        }

        // Known IDs
        if (element.id().equals("cf-hcaptcha-container") || element.id().equals("g-recaptcha-response")
                || element.id().equals("audioCaptchaArea") || element.id().equals("recaptcha-anchor-label")) {
            return true;
        }

        if (element.tag().equals(Tag.valueOf("iframe"))) {
            if (element.attr("title").equalsIgnoreCase("reCAPTCHA")) {
                return true;
            }

            if (element.attr("title").toLowerCase().contains("hcaptcha")) {
                return true;
            }
        }

        return false;
    }

}
