/*
 *     RPCrawler
 *     Copyright (C) 2021  Jurgen Mulder
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.simgar98.crawler.util;

import com.simgar98.crawler.scheduler.CrawlerAsyncScheduler;
import lombok.Getter;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Logger {

	private PrintWriter out;
	private HashMap<String, String> colorHolders;
	private LinkedBlockingQueue<String> logRecord;
	private int logNr;
    private SimpleDateFormat format;
    @Getter private ExecutorService pool;
    private static Logger instance;
	
	private Logger() {
		logNr = 0;
		logRecord = new LinkedBlockingQueue<>();
		format = new SimpleDateFormat("HH:mm:ss");
		initColorHolders();

        pool = new ThreadPoolExecutor(1, 1, 69L, TimeUnit.HOURS, new LinkedBlockingQueue<>());
		
		File logsFolder = new File("logs");
		if (logsFolder.isDirectory()) {
			for (File file : Objects.requireNonNull(logsFolder.listFiles())) {
			    if (file.isDirectory()) {
			        continue;
                }

			    String f = file.getName();
				String name = f.replace(".log", ""); // Not perfect, but good enough for us.
				String rawAmount = name.replace("log-", "");
				int amount = Integer.parseInt(rawAmount);
				
				if (amount >= logNr) {
					logNr = amount + 1;
				}
			}
		} else {
			Logger.log("Creating logs folder: " + logsFolder.mkdirs());
		}

        CrawlerAsyncScheduler.submitRepeating(this::save, 1000L);
	}

    private void save() {
	    if (out == null) {
            initWriter();
        }

	    List<String> drainedLog = new ArrayList<>();
	    logRecord.drainTo(drainedLog);
	    for (String s : drainedLog) {
	        out.println(s);
        }

        out.flush();

        if (logRecord.size() > 4096) {
            closeWriter();
        }
    }

    private void initColorHolders() {
		colorHolders = new HashMap<String, String>();
		
		String ansiBase = "\033[0;3";
		colorHolders.put("@BLACK", ansiBase + "0m");
		colorHolders.put("@RED", ansiBase + "1m");
		colorHolders.put("@GREEN", ansiBase + "2m");
		colorHolders.put("@YELLOW", ansiBase + "3m");
		colorHolders.put("@BLUE", ansiBase + "4m");
		colorHolders.put("@PURPLE", ansiBase + "5m");
		colorHolders.put("@CYAN", ansiBase + "6m");
		colorHolders.put("@WHITE", ansiBase + "7m");
		colorHolders.put("@RESET", "\033[0m");
	}
	
	private void initWriter() {
		try {
			File file = new File("logs/log-" + logNr + ".log");
			if (!file.exists()) {
				file.createNewFile();
			}
			
			out = new PrintWriter(file);
		} catch (IOException e) {
			System.out.println("Something went wrong while initializing the logger!");
			e.printStackTrace();
		}
	}
	
	private void closeWriter() {
		out.close();
		out = null;
		logNr++;
		logRecord.clear();
	}
	
	private void logMsg(String rawMsg, boolean dontSave) {
	    long timestamp = System.currentTimeMillis();
        getPool().submit(() -> {
            String timeStamp = format.format(new Date(timestamp));
            String msg = "[" + timeStamp + "] " + rawMsg;

            if (!dontSave) {
                String colorLess = msg;
                for (String key : colorHolders.keySet()) {
                    colorLess = colorLess.replace(key, "");
                }

                logRecord.add(colorLess);
            }

            msg = "@CYAN" + msg + "@RESET";

            for (Entry<String, String> repl : colorHolders.entrySet()) {
                msg = msg.replace(repl.getKey(), repl.getValue());
            }

            System.out.println(msg);
        });
	}
	
	public static void log(String msg, boolean dontSave) {
		getInstance().logMsg(msg, dontSave);
	}
	
	public static void log(String msg) {
		log(msg, false);
	}

	public static void log(Exception e) {
		log("@RED Error incoming!", false);
		log("@REDError: " + e, false);
		for (StackTraceElement traceElement : e.getStackTrace()){
			log("@REDat: " + traceElement, false);
		}
	}

	public static Logger getInstance() {
	    if (instance == null) {
	        instance = new Logger();
        }

	    return instance;
    }

}
