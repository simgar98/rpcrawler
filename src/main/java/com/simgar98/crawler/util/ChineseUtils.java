/*
 *     RPCrawler
 *     Copyright (C) 2021  Jurgen Mulder
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.simgar98.crawler.util;

public class ChineseUtils {

    public static int countIdeoGraphicCharacters(String text) {
        int chineseCount = 0;
        char[] chars = text.toCharArray();
        for (char c : chars) {
            if (Character.isIdeographic(c)) {
                chineseCount++;
            }
        }

        return chineseCount;
    }

    // Avoids some falseflags
    public static boolean isConfirmedNotAsian(String url) {
        String domain = LinkUtils.getDomain(url);
        if (domain.equalsIgnoreCase("trello.com")) {
            return true;
        }

        if (domain.equalsIgnoreCase("shein.com")) {
            return true;
        }

        if (domain.equalsIgnoreCase("ecer.com")) {
            return true;
        }

        return false;
    }

}
