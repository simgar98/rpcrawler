/*
 *     RPCrawler
 *     Copyright (C) 2021  Jurgen Mulder
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.simgar98.crawler.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import static com.simgar98.crawler.util.Logger.log;

public class LinkUtils {

    private final static List<String> noHttps = Arrays.asList("go.com", "royalbank.com", "addthis.com",
            "cambridge.org");
    private final static List<String> blacklisted = Arrays.asList("bet365.com", "ikea.com", "geeksforgeeks.org", "myworkdayjobs.com",
            "cloudfront.net", "bbcollab.com", "banvenez.com", "akamaized.net", "9384.com", "twimg.com", "thepiratebay.org",
            "laroza.net");
    private final static List<String> needsExtraTime = Arrays.asList("samsung.com");

    public static String getDomain(String url) {
        return url.replaceAll("^http[s]*://", "") // Replaces the http(s):// at the start
                .replace("www.", "") // Dont care about www.
                .replaceAll("/.*$", "") // Replaces the /<anything> at the end
                .replaceAll("\\?.*", ""); // Replaces any queries (if none were removed by trailing /
    }

    public static List<String> readFileToList(File file) {
        List<String> urls = new ArrayList<>();
        try (FileReader fileReader = new FileReader(file);
             BufferedReader reader = new BufferedReader(fileReader)) {
            String line;
            while ((line = reader.readLine()) != null) {
                urls.add(line);
            }
        } catch (IOException e) {
            log("@REDSomething went wrong reading list from file");
            log(e);
        }

        return urls;
    }

    public static boolean isSameDomain(String link, String domain) {
        String linkDomain = LinkUtils.getDomain(link).toLowerCase();
        return linkDomain.endsWith(domain.toLowerCase());
    }

    public static boolean isGoodLink(String link) {
        String domain = getDomain(link);
        String[] split = link.split(Pattern.quote(domain));
        if (split.length <= 1) {
            return false;
        }

        String page = split[1];
        if (page.startsWith("#") || page.startsWith("/#") || link.endsWith("#")) {
            return false;
        }

        if (link.replaceAll("://", "").contains(":")) {
            return false;
        }

        if (link.contains("javascript")) {
            return false;
        }

        return true;
    }

    public static String fixLink(String link, String domain) {
        if (link.startsWith("http") || link.startsWith("www.")) {
            return link;
        }

        return domain + (link.startsWith("/") ? "" : "/") + link;
    }

    public static String fixUrl(String url) {
        if (!url.startsWith("http")) {
            url = "http" + (isNoHttps(url) ? "" : "s") + "://" + url;
        }

        if (isBlacklistedDomain(url)) {
            Logger.log("@REDException: Domain is blacklisted of URL " + url + ", redirecting to google.com");
            return "https://google.com";
        }

        return url;
    }

    private static boolean isNoHttps(String url) {
        String domain = getDomain(url).toLowerCase();
        for (String d : noHttps) {
            if (domain.contains(d)) {
                return true;
            }
        }

        return false;
    }

    public static boolean isBlacklistedDomain(String url) {
        String domain = getDomain(url).toLowerCase();
        for (String d : blacklisted) {
            if (domain.contains(d)) {
                return true;
            }
        }

        return false;
    }

    public static boolean isNeedsExtraTime(String url) {
        String domain = getDomain(url).toLowerCase();
        for (String d : needsExtraTime) {
            if (domain.contains(d)) {
                return true;
            }
        }

        return false;
    }

    public static String removeSubDomain(String nextDomain) {
        return nextDomain.replaceAll("^[^.]*\\.", "");
    }

}
