/*
 *     RPCrawler
 *     Copyright (C) 2021  Jurgen Mulder
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.simgar98.crawler;

import com.simgar98.crawler.scrape.ScraperProfile;
import lombok.Getter;
import lombok.Setter;

public class Constants {

    /*
    In case anyone reading this thinks it is bad practice to store DB credentials hardcoded and/or in git:
    These credentials (and any in the history) are for a localhost only MariaDB db, and the user:password is for
    a user limited to only log in on localhost. Yes, it is generally speaking bad practice to store this,
    but in this case it does not matter. Yes, this could have easily been moved to a config file.
     */
    @Getter private static String MYSQL_HOST = "localhost";
    @Getter private static int MYSQL_PORT = 3306;
    @Getter private static String MYSQL_DATABASE = "rpcrawler";
    @Getter private static String MYSQL_USER = "rpcrawleruser";
    @Getter private static String MYSQL_PASSWORD = "sdf9128743khasf85sfal8";

    @Getter @Setter private static int EXPERIMENT_JAP_ID = 1;
    @Getter @Setter private static ScraperProfile EXPERIMENT_PROFILE = ScraperProfile.FIREFOX_REGULAR_SLOW;

    /*
    Parameters for excluding Asian sites.
     */
    @Getter private static int MAX_IDEOGRAPHIC_CHARACTERS = 300;
    @Getter private static int MAX_IDEOGRAPHIC_CHARACTERS_TITLE = 3;

    /*
    Parameters for the selenium instance, timeouts, webdriver, cache, lazyloading.
     */
    @Getter private static int SELENIUM_LOAD_TIMEOUT_SECONDS = 120;
    @Getter private static int SELENIUM_JS_TIMEOUT_SECONDS = 10;
    @Getter private static String SELENIUM_WEBDRIVER_FIREFOX = "geckodriver-v0.29.1-linux64";
    @Getter private static boolean SELENIUM_USE_CACHE = true;
    @Getter private static boolean SELENIUM_USE_LAZYLOADING = true;

    /*
    Parameters for the analyzing/categorizing.
     */
    @Getter private static int ANALYSIS_THREADS = 8;
    @Getter private static double DOM_LENGTH_CUTOFF = 0.50;
    @Getter private static double DOM_SIMILARITY_SIMPLE_CUTOFF = 0.70;
    @Getter private static int DOM_MAX_KEYWORD_DIFF = 1;
    @Getter private static double TEXT_LENGTH_CUTOFF = 0.02;
    @Getter private static double EMPTY_MAX_DOM_SIMILARITY = 0.08;
    @Getter private static double IMAGE_DIFFERENCE_PERCENT_LIMIT = 0.25;
    @Getter private static double IMAGE_DIFFERENCE_HASH_LIMIT = 0.15;
    @Getter private static double SUMMARY_WORST_CATEGORY_LIMIT = 0.95;

    /*
    Parameters for the FIREFOX_REGULAR_SLOW ScraperProfile, in bytes per second and milliseconds
     */
    @Getter private static long SLOW_PROXY_DOWNSTREAM_LIMIT = 100 * 1024 / 8;
    @Getter private static long SLOW_PROXY_UPSTREAM_LIMIT = 100 * 1024 / 8;
    @Getter private static long SLOW_PROXY_LATENCY = 600;

}
